# README #

We investigate a new approach for evaluating session-based information retrieval systems, based on the application of Markov chains. In particular, we develop a new family of evaluation measures, inspired by random walks, which accounts for the probability of moving to the next and previous documents in a result list, to the next query in a session, and to the end of the session. We leverage this Markov chain to substitute what in other measures is a somehow fixed discount linked to the rank of a document or to the position of a query in a session with a stochastic average time to reach a document and the probability of actually reaching a given query.
We experimentally evaluate our new family of measures with respect to state-of-the-art ones -- namely, session DCG, Cube Test, and Expected Utility -- over standard TREC collections, i.e. the Session Track and Dynamic Domain Track ones.

### What is this repository for? ###

1. SIGIR Paper
2. Data
3. Experiments Code


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

David van Dijk <d.v.van.dijk@hva.nl>
Marco Ferrante <ferrante@math.unipd.it>
Nicola Ferro <ferro@dei.unipd.it> 
Evangelos Kanoulas <e.kanoulas@uva.nl>