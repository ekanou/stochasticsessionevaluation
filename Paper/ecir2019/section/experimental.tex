% \clearpage

\section{Experimental Setup}
\label{sec:Experimental_Setup}

In this section, we evaluate the behavior of the proposed measure, answering the following research questions: 
\RQ{1}{How does $MsM$ compare to existing session evaluation measures regarding the ranking of retrieval systems?}
\RQ{2}{Which factors of the user dynamics affect these correlations and to what extent?}
%\RQ{3}{How does the proposed measure correlate with user session satisfaction?}

\subsubsection{Computation of the MsM Measure}

We developed an efficient way of computing the $MsM$ measure, avoiding the most general and immediate approach of using a large block-diagonal matrix, where each sub-matrix would represent a single query in the session. For space reasons the pseudo-code of the algorithm is omitted here but it is available in the electronic appendix available as Online Resource 1. Moreover, to further ease the reproducibility of experiments, the source code of the actual implementation is available at: \url{https://github.com/ekanou/Markovian-Session-Measures}.

\subsubsection{Data Collection}
\label{subsubsec:Data_Collections}
To answer \RQRef{1} and \RQRef{2} we ran experiments on the TREC 2015, 2016, and 2017 Dynamic Domain Track (DDT) collection. The search tasks in DDT focus in domains of special interests, which usually produce complex and exploratory searches with multiple rounds of user and search engine interactions. The DDT collection consists of a set of topics, and multi-query sessions corresponding to each topic. %Table~\ref{tab:dd_stats} show some basic statistics for the test collections.
In DDT retrieval systems were provided with the first query, they returned a ranked list of 5 documents, and based on passage annotations in these documents, a jig (user simulation) returned a follow-up query. IR systems had the chance to decide when to stop providing users with ranked lists of documents.

%To answer \RQRef{3} we use the logs of the user study performed by Liu et. al~\cite{LiuEtAl2018}, which includes search logs of multi-query sessions, with clicks and annotations of the clicked documents, as well as a user defined score in the range of 1 to 5 of session level satisfaction. 

%\begin{table}[tb]
%\centering
%\caption{Dynamic Domain datasets statistics.}% including the number of systems that participated, the number of multi-query sessions, the average number of queries per session (including the standard deviation and the median), and the average number of relevant documents per topic (including the standard deviation and the median).}
%\label{tab:dd_stats}
%\small
%\begin{tabular}{L{2.8cm}@{\hskip .5cm}C{2.8cm}@{\hskip .5cm}C{2.5cm}@{\hskip .5cm}C{2.5cm}}
%\toprule
%\textbf{Dynamic Domain}         & 2015    & 2016 & 2017\\ 
%\midrule
%Systems			       	& 22      & 21	 &	11 \\
%Sessions               	        & 118     & 53   &  60 \\			  
%Queries per session             & 32 $\pm$ 108;M=6 & 5 $\pm$ 6.56;M=3  & 6 $\pm$ 2.14;M=5\\ 
%Rel docs per topic    & 497 $\pm$ 1219;M=146  &    291 $\pm$ 546;M=63   &    63 $\pm$ 70;M=30\\ 
%\bottomrule
%\end{tabular}
%\end{table}

\subsubsection{Session Evaluation Measures}
\label{subsec:Evaluation_Metrics}
We compare $MsM$ to the normalized~\cite{Tang:2017} versions of session DCG (sDCG)~\cite{Jarvelin:2008}, Expected Utility (EU)~\cite{Yang:2009}, and Cube Test (CT)~\cite{Luo:2013}. Since we are not dealing with diversity, we simplify them by using a gain function that ignores subtopic relevance.

\subsubsection{Model Instantiations.}
\label{subsec:Inst}

As an exemplification for experimentation purposes, we consider two user's models, with two different set of assumptions. The first model, called {\bf Random-Walk model}, assumes a user who after considering a document she decides, according to constant probabilities, to proceed to the next document (p), to the previous document (q), to stop her search (s), or to reformulate a new query (r). From the transition matrix point of view, this model is determined by the following assumptions:
\begin{equation*}
\label{RWModel}
\begin{array}{l}
p_{(i,j),(i+1,j)}=p \ \ \mbox{if} \ \ i\neq F
\\
p_{(i,j),(i-1,j)}=q \ \ \mbox{if} \ \ i\neq 1
\\
p_{(i,j),(1,j+1)}=r \ \ \mbox{if} \ \ i\neq F
\\
p_{(i,j),(F,j)}=s \ \ \mbox{if} \ \ i\neq F
\\
p_{(F,j),(F,j)}=1 \ \ \mbox{for any} \ \ j
\end{array}
\end{equation*}
% \begin{equation}
% \label{RWModel}
% \begin{array}{l}
% p_{(i,j),(i+1,j)}=p \ \ \mbox{if} \ \ i\neq F
% \\
% \textcolor{red}{p_{(i,j),(i-1,j)}=q \ \ \mbox{if} \ \ i\neq 1}
% \\
% p_{(i,j),(1,j+1)}=r \ \ \mbox{if} \ \ i\neq F
% \\
% p_{(i,j),(F,j)}=s \ \ \mbox{if} \ \ i\neq F
% \\
% p_{(F,j),(F,j)}=1 \ \ \mbox{for any} \ \ j
% \end{array}
% \end{equation}
% \begin{equation}
% \label{RWModel}
% \begin{array}{l}
% p_{(i_n,j_n),(i_{n}+1,j_{n})}=p_{nd} \ \ \mbox{if} \ \ i_n\neq F
% \\
% p_{(i_n,j_n),(i_{n}-1,j_{n})}=p_{pd} \ \ \mbox{if} \ \ i_n\neq 1
% \\
% p_{(i_n,j_n),(1,j_{n}+1)}=p_{nq} \ \ \mbox{if} \ \ i_n\neq F
% \\
% p_{(i_n,j_n),(F,j_{n})}=p_{end} \ \ \mbox{if} \ \ i_n\neq F
% \\
% p_{(F,j_n),(F,j_{n})}=1 \ \ \mbox{for any} \ \ j_{n}
% \end{array}
% \end{equation}
where $p+q+r+s=1$ and $p>0$, $q\ge0$, $r>0$ and $s>0$.

The second one, called {\bf Forward model}, is a special case of the first one, inspired by the RBP philosophy, where the backward probability, $q$, is set to 0, i.e. it assumes that the user moves only forward in the ranked list. 

\subsubsection{Experiments}
\label{subsec:Exp}

To answer \RQRef{1} we experimented with both the Forward and the Random-Walk model, using reciprocal log and linear weight, introduced earlier, while the probabilities $p$, $r$, and $s$ were set to values on a grid in $[0, 1)^3$ with a step of $0.05$, under the constraint that $p + r + s = 1$. Given that the results of the Forward and Random-walk model were highly correlated, and due to space limitations, we present results only of the Forward model.


To answer \RQRef{2} we experiment with both the Forward and Random-Walk model, while we factorize user types by three characteristics: (a) {\em patience}, in terms of the total number of documents they are willing to examine, (b) {\em browsing pattern} in terms of whether they prefer to scan the ranked list or reformulate, and (c) {\em decisiveness} in terms of deciding whether a document is relevant once they observe it, or moving back to it and re-examining it after they have examined more documents. We control patience by setting the stopping probability, $s$, to three distinct values, $0.01$, $0.1$, and $0.3$; the first type of user will on average view around $50$ documents, the second around $9$, and the third around $3$, before they quit their search. We control the browsing pattern by setting the probabilities of walking down the ranked list, $p$, and reformulating, $r$ to a set of values, such that the user either demonstrates a bigger willingness to scan the ranked list, to reformulate, or to have a balanced behaviour. Last, we control decisiveness by either not allowing the user to walk backwards, hence setting the backwards probability, $q$, to $0$, or allowing the user to do so, by setting $q$ to $0.1$. \footnote{Advanced user dynamics that condition probabilities on the relevance of the viewed document, similar to ERR, are also possible with $MsM$ but are left as future work.}

\if [0]
The set of configurations in terms of the $p$, $q$, $r$, and $s$ probabilities, is summarized in the table below. 
\begin{center}
\small
\begin{tabular}{llllll}
\hline
Measure & $p$ & $q$ & $r$ & $s$ \\ % & weight \\
\hline
MsM-fwd-imp-down &  0.6& 0.0& 0.1& 0.3\\
MsM-fwd-imp-bal &  0.35& 0.0& 0.35& 0.3\\
MsM-fwd-imp-reform &  0.1& 0.0& 0.6& 0.3\\
MsM-fwd-bal-down &  0.8& 0.0& 0.1& 0.1\\
MsM-fwd-bal-bal &  0.45& 0.0& 0.45& 0.1\\
MsM-fwd-bal-reform &  0.1& 0.0& 0.8& 0.1\\
MsM-fwd-pat-down &  0.9& 0.0& 0.09& 0.01\\
MsM-fwd-pat-bal &  0.5& 0.0& 0.49& 0.01\\
MsM-fwd-pat-reform &  0.1& 0.0& 0.89& 0.01\\
\hline
\end{tabular}
\hspace*{2ex}
\begin{tabular}{llllll}
\hline
Measure & $p$ & $q$ & $r$ & $s$ \\ % & weight \\
\hline
MsM-rnd-imp-down &  0.5& 0.1& 0.1& 0.3\\
MsM-rnd-imp-bal &  0.3& 0.1& 0.3& 0.3\\
MsM-rnd-imp-reform &  0.1& 0.1& 0.5& 0.3\\
MsM-rnd-bal-down &  0.7& 0.1& 0.1& 0.1\\
MsM-rnd-bal-bal &  0.4& 0.1& 0.4& 0.1\\
MsM-rnd-bal-reform &  0.1& 0.1& 0.7& 0.1\\
MsM-rnd-pat-down &  0.8& 0.1& 0.09& 0.01\\
MsM-rnd-pat-bal &  0.45& 0.1& 0.44& 0.01\\
MsM-rnd-pat-reform &  0.1& 0.1& 0.79& 0.01\\
\hline
\end{tabular}
\end{center}
\fi
