% \clearpage

\section{Experimental Setup}
\label{sec:Experimental_Setup}

In this section, we evaluate the behavior of the proposed measure, answering the following research questions\footnote{To ease the reproducibility of the experiments, all the source code is available at \url{http:\\www.anonym.ized}.}: 
%\RQ{1}{What are examples of interesting instantiations of $MsM$ that arise from different assumptions over user behaviors?}
\RQ{1}{How does $MsM$ compare to existing session evaluation measures regarding the ranking of retrieval systems?}
\RQ{2}{How does $MsM$ compare to existing session evaluation measures regarding their ability to discriminate runs in a comparative evaluation setup?}
% We use terms 'session' and 'run' for the same thing. 

% \subsection{Research Questions}
% \label{subsec:Research_Questions}


\subsection{Data Collections}
\label{subsec:Data_Collections}
We run experiments using the data collections produced by the TREC Dynamic Domain Track in 2015, 2016, and 2017. The three data collections provide a set of topics and multi-query sessions corresponding to each topic. Table~\ref{tab:dd_stats} show some basic statistics for the test collections. In the dynamic domain track retrieval systems are provided with the first query, they return a ranked list of 5 documents, and based on passage annotations of these documents, a jig (user simulation) returns a follow-up query. Retrieval systems also have the chance to decide when to stop providing users with ranked lists of documents (i.e. different session abandonment points). 


\subsection{Model Instantiation}
\label{subsec:Inst}
We experiment with the Forward and Random-Walk models, introduced above, in combination with both the reciprocal weight and the reciprocal logarithmic weight for several alternatives of the $p$, $q$, $r$, and $s$ probabilities, as summarized in the table below. 
\vspace*{1ex}
\begin{center}
\small
\begin{tabular}{lccccc}
\hline
Measure & $p$ & $q$ & $r$ & $s$ \\ % & weight \\
\hline
MsM-fwd-imp-down &  0.6& 0.0& 0.1& 0.3\\
MsM-fwd-imp-bal &  0.35& 0.0& 0.35& 0.3\\
MsM-fwd-imp-reform &  0.1& 0.0& 0.6& 0.3\\
MsM-fwd-bal-down &  0.8& 0.0& 0.1& 0.1\\
MsM-fwd-bal-bal &  0.45& 0.0& 0.45& 0.1\\
MsM-fwd-bal-reform &  0.1& 0.0& 0.8& 0.1\\
MsM-fwd-pat-down &  0.9& 0.0& 0.09& 0.01\\
MsM-fwd-pat-bal &  0.5& 0.0& 0.49& 0.01\\
MsM-fwd-pat-reform &  0.1& 0.0& 0.89& 0.01\\
MsM-bck-imp-down &  0.5& 0.1& 0.1& 0.3\\
MsM-bck-imp-bal &  0.3& 0.1& 0.3& 0.3\\
MsM-bck-imp-reform &  0.1& 0.1& 0.5& 0.3\\
MsM-bck-bal-down &  0.7& 0.1& 0.1& 0.1\\
MsM-bck-bal-bal &  0.4& 0.1& 0.4& 0.1\\
MsM-bck-bal-reform &  0.1& 0.1& 0.7& 0.1\\
MsM-bck-pat-down &  0.8& 0.1& 0.09& 0.01\\
MsM-bck-pat-bal &  0.45& 0.1& 0.44& 0.01\\
MsM-bck-pat-reform &  0.1& 0.1& 0.79& 0.01\\
\hline
\end{tabular}
\end{center}
\vspace*{1ex}

Each afore-mentioned configuration models a user type. We factorize user types by three characteristics: (a) {\em patience}, in terms of the total number of documents they are willing to examine, (b) {\em browsing pattern} in terms of whether they prefer to scan the ranked list or reformulate, and (c) {\em decisiveness} in terms of deciding whether a document is relevant once they observe it or moving back to it and re-examining it after they have examined more documents. We control patience by setting the stopping probability, $s$, to three distinct values, $0.01$, $0.1$, and $0.3$; the first type of user will on average view around $50$ documents, the second around $9$, and the third around $3$, before they quit their search. We control the browsing pattern by setting the probabilities of walking down the ranked list, $p$, and reformulating, $r$ to a set of values, such that the user either demonstrates a bigger willingness to scan the ranked list, or to reformulate, or a balanced behaviour with the two probabilities set to the same value. Last, we control decisiveness by either not allowing the user to walk backwards, hence setting the backwards probability, $q$, to $0$, or allowing the user to do so, by setting $q$ to $0.1$. Note that these are only few examples of the possible instantiations of the $MsM$ measure. Advanced user dynamics that condition probabilities on the relevance of the viewed document, similar to ERR, are also possible.

% This figure is out of place only so it appears at the right spot on the pdf.
\begin{figure*}[t!]
  \centering
  \begin{tabular}{C{8cm}C{10cm}}
    \includegraphics[width=1\linewidth]{img/dd_all_only_lin_fwd_all__kendall.png} \vspace{1.3cm} &
    \includegraphics[width=1\linewidth]{img/dd_all_yes_lin_fwd_all__kendall.png} \\
    \textbf{(a) All versions of existing measures.} & \textbf{(b) MsM with linear reciprocal weight.} \\
  \end{tabular}
  \caption{Average Kendall's $\tau$ correlation heatmap.}
  \label{fig:Kendall}
\end{figure*}

\subsection{Session Evaluation Measures}
\label{subsec:Evaluation_Metrics}
The session measures used for comparison purposes are the session-based DCG (sDCG)~\cite{Jarvelin:2008}, the Expected Utility (EU)~\cite{Yang:2009}, and the Cube Test (CT)~\cite{Luo:2013}, along with their normalized versions~\cite{Tang:2017} (denoted with a prefix 'n' in the results section). To make existing measures comparable to $MsM$, we simplify them by using a gain function that ignores subtopic relevance (denoted by the suffix 's' in the results section).
