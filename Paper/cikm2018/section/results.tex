\section{Results and Analysis}
\label{subsec:Results_and_Analysis}

To answer \RQRef{1}, we conduct a correlation analysis using Kendall’s $\tau$~\cite{Kendall1948} among the rankings of systems produced by the different evaluation measures. \citet{Ferro2017} has shown that, even if the absolute correlation values are different, removing or not the lower quartile runs produces the same ranking of measures in terms of correlation; similarly, it was shown that both $\tau$ and AP correlation $\tau_{ap}$~\cite{YilmazEtAl2008} produce the same ranking of measures in terms of correlation. Therefore, we focus only on Kendall's $\tau$ without removing lower quartile systems. 
%
To answer \RQRef{2} we calculate the \acf{DP} of each measure using a paired bootstrap test~\cite{Sakai:2006}.

\subsection{Correlation Analysis}
\label{subsubsec:Correlation_Analysis}

Figure~\ref{fig:Kendall}a presents the Kendall's $\tau$ correlation values for different variations of the existing session measures, including their original formulations, their normalized versions and their simplified, by removing subtopic relevance, versions\footnote{Having observed similar trends across the three years of the Dynamic Domain track we present the average over the three years Kendall's $\tau$ values.}. We observe the following:
\begin{description}[topsep=0pt,nosep=0pt, leftmargin=0cm, noitemsep,nolistsep]
\item[1.] There is a strong correlation between the simplified and original version of the measures, which ranges from 0.83 to 1, justifying comparisons between MsM and the simplified metrics.
\item[2.] There is a strong correlation between the normalized and original version of the measures, which ranges from 0.78 to 0.91. If we examine the results for each individual year, the EU varies the most with the lowest correlation being 0.67 in 2017, which justifies the need for normalization proposed by ~\citet{Tang:2017}.
\item[3.] The Cube Test appears to strongly disagree with all other measures with respect to system rankings. This is probably due to the way cost is calculated in CT, since it discounts the overall gain and not the gain per document.
\item[4.] SDCG demonstrates a mid-range positive correlation with EU.  
\end{description}

Next, we focus on the correlation of system rankings produced by the Forward and Random-Walk models of MsM. The Kendall's $\tau$ correlations between the two models for all {\em patience} and {\em browsing pattern} configurations are shown in Table~\ref{table:Fwd-Bck}. All correlation values are greater than 0.9. Hence, in what follows we focus on the Forward model only.

\begin{table*}[tb]
\caption{Kendall's $\tau$ correlations of the system rankings produced by the Forward and Random-Walk MsM models.}
\label{table:Fwd-Bck}
\small
\begin{tabular}{ll|ccccccccc}
\toprule
Patience&	& \multicolumn{3}{c}{impatient} & \multicolumn{3}{c}{balanced}  & \multicolumn{3}{c}{patient} \\
\midrule
Browsing&    & down & balanced & reform & down & balanced & reform & down & balanced & reform\\
\midrule
\multirow{ 2}{*}{Reciprocal}
&   Linear         & 0.98 & 0.96 & 0.90 & 0.94 & 0.99 & 0.94 & 0.96 & 1.00 & 0.99 \\
&   Log         & 0.97 & 1.00 & 1.00 & 0.99 & 0.99 & 0.98 & 0.99 & 1.00 & 0.99 \\
\bottomrule
\end{tabular}
\end{table*}


Figure~\ref{fig:Kendall}b presents the average over all years Kendall's $\tau$ correlation values for the linear reciprocal weight of the MsM measure. Given the strong correlation between the different variations of the sDCG, EU and CT only the simplified normalized measures have included for comparison. We make the following observations:

\begin{description}[topsep=0pt,nosep=0pt, leftmargin=0cm, noitemsep,nolistsep]
\item[5.] The different versions of MsM are all correlated with each other, to some extent. In particular: 
\begin{description}[topsep=0pt,nosep=0pt, leftmargin=0cm, noitemsep,nolistsep]
\item[5a.] There is a positive correlation between the different models of user's browsing pattern (shown in cells surrounded by a blue rectangle in Figure~\ref{fig:Kendall}). The Kendall's $\tau$ between the user that tends to walk down the ranked list and the one that tends to reformulate is the smallest, as expected, and equal to 0.74, 0.74, and 0.76 for the three levels of user patience and the linear reciprocal weight, and 0.76, 0.72, 0.95 for the three levels of user patience and the log reciprocal weight (not shown in the heatmap), respectively.
\item[5b.] There is a positive correlation between the different models of user's patience (shown in cells surrounded by a magenta rectangle in Figure~\ref{fig:Kendall}). The Kendall's $\tau$ between patient and impatient users is the smallest, as expected, and equal to 0.78, 0.74, and 0.8 for the three models of browsing pattern and the linear reciprocal weight, and 0.62, 0.75, 0.81 for the three models of browsing pattern and the log reciprocal weight (not shown in the heatmap), respectively.
\end{description}
\item[6.] The MsM models appear to have a high correlation with nsDCGs, a mid-size correlation with nEUs, and a zero or negative correlation with nCTs. 
\item[7.] The correlation between MsM and nsDCGs for the linear reciprocal weight increases as the user is modeled to reformulate more. The exact opposite is true for the log reciprocal weight. Hence to some extend the nsDCGs models a user that tends to reformulate but penalizes relevance in later queries mildly.
\item[8.] The correlation between MsM and nEUs does not demonstrate a consistent pattern across different user types when we consider the average correlation. However, when we inspect each year independently, we observe that in 2015 the more patient the user is and the more she reformulates the stronger the correlation, with a maximum of 0.84. On the other hand in 2016, the less patient the user is and the less she reformulates the higher the correlation, with a maximum as small as 0.31. This demonstrates that the MsM and EU models are different and correlate only when relevant documents appear in specific positions. %For instance in 2015 most of the earlier the query in the session and the lower the rank the bigger the chance of a user to find a relevant document, while in 2016 the effectiveness of later queries in the ranking was almost equal to the earlier queries. 
\end{description}

%While we leave the reduction of MsM to the existing measures as a future work, we can still make some sense of the existing measures in terms of their correlations with the different instantiations of MsM. Overall, it seems that sDCG incorporate a user model in which reformulating queries is to a large extent not penalized much, given the strong correlation with MsM instantiations that model users who tend to read many documents but also reformulate.



\begin{table}[tb]
\centering
\caption{ANOVA analysis of the factors influencing correlation with respect to nsDCGs.}
\label{tab:anova_corr_nsDCGs}
\small
	\begin{tabular}{|l|r|r|r|r|r|r|}
		\hline
		\multicolumn{1}{|c}{\textbf{Source}} & \multicolumn{1}{|c}{\textbf{SS}} & \multicolumn{1}{|c}{\textbf{DF}} & \multicolumn{1}{|c}{\textbf{MS}} & \multicolumn{1}{|c}{\textbf{F}} & \multicolumn{1}{|c}{\textbf{p}} & \multicolumn{1}{|c|}{$\hat{\omega}_{\langle fact \rangle}^2$} \\
		\hline
		\textbf{Track}		& 0.1526	& 2		& 0.0763	& 5.3294	& 0.0083	& 0.1382 \\
		\hline
		\textbf{Patience}	& 0.2712	& 2		& 0.1356	& 9.4704	& 0.0003	& 0.2388 \\
        \hline
		\textbf{Browsing}	& 0.4330	& 2		& 0.2165	& 15.1245	& < e-4		& 0.3435 \\
		\hline
		\textbf{Weight}		& 0.0014	& 1		& 0.0014	& 0.1004	& 0.7528	& -- \\
    	\hline
		\textbf{Error}		& 0.6582	& 46	& 0.0143	& 			& 			& \\
		\hline			
		\textbf{Total}		& 1.5167	& 53	& 			& 			& 			& \\
		\hline
	\end{tabular}
\end{table}    

Table~\ref{tab:anova_corr_nsDCGs} shows the three-way \ac{ANOVA} for analysing the factors in the MsM measures which influence the correlation with nsDCGs. The Track factor represents the effect of one of the three tracks (DD 2015, 2016, and 2017); the Patience factor represents the effect of the patience of the user in scanning the list (impatient, balanced, patient); the Browsing factor represents the attitude to walk down the list or reformulate new queries (down, balanced, reformulate); the Weight factor represents the type of discount (linear, log). The \ac{ANOVA} analysis shows that the Track, Patience, and Browsing factors are statistically significant while the Weight one is not; we also conducted an \ac{ANOVA} analysis (not reported here for space reason) to test the interaction among these factors but none of them is significant. The Tukey \ac{HSD} test shows that the \texttt{impatient} user is significantly different from the \texttt{balanced} and \texttt{patient} ones, which are not significantly different from each other, being the \texttt{impatient} user the lowest one in terms of correlation and the \texttt{patient} the highest one. The Tukey \ac{HSD} test also shows that the \texttt{balanced} browsing pattern is significantly different from the \texttt{down} and \texttt{reformulate} ones, which are not significantly different from each other, being the \texttt{reformulate} strategy the lowest one in terms of correlation and the \texttt{balanced} the highest one. The \ac{SOA} $\omega^2$ shows that the Track factor  is a medium-size effect while the Patience and Browsing factors are large-size effects, being the browsing pattern the most prominent one. Overall, this analysis suggests that the most prominent motivations of similarity between MsM measures and nsDCGs are a balanced browsing pattern and a balanced/patient user, which are the user model actually implemented in nsDCGs.


\begin{table}[tb]
\centering
\caption{ANOVA analysis of the factors influencing correlation with respect to nEUs.}
\label{tab:anova_corr_nEUs}
\small
	\begin{tabular}{|l|r|r|r|r|r|r|}
		\hline
		\multicolumn{1}{|c}{\textbf{Source}} & \multicolumn{1}{|c}{\textbf{SS}} & \multicolumn{1}{|c}{\textbf{DF}} & \multicolumn{1}{|c}{\textbf{MS}} & \multicolumn{1}{|c}{\textbf{F}} & \multicolumn{1}{|c}{\textbf{p}} & \multicolumn{1}{|c|}{$\hat{\omega}_{\langle fact \rangle}^2$} \\
		\hline
		\textbf{Track}		& 0.0084	& 2		& 0.0042	& 0.3962	& 0.6752	& -- \\
		\hline
		\textbf{Patience}	& 0.0255	& 2		& 0.0127	& 1.2022	& 0.3098	& -- \\
        \hline
		\textbf{Browsing}	& 0.9185	& 2		& 0.4593	& 43.3024	& < e-4		& 0.6104 \\
		\hline
		\textbf{Weight}		& 0.0057	& 1		& 0.0057	& 0.5333	& 0.4689	& -- \\
    	\hline
		\textbf{Error}		& 0.4879	& 46	& 0.0106	& 			& 			& \\
		\hline			
		\textbf{Total}		& 1.4459	& 53	& 			& 			& 			& \\
		\hline
	\end{tabular}
\end{table}    

Table~\ref{tab:anova_corr_nEUs} shows the three-way \ac{ANOVA} for analysing the factors in the MsM measures which influence the correlation with nEUs. The \ac{ANOVA} analysis shows that only the Browsing factor is statistically significant while all the others are not; we also conducted an \ac{ANOVA} analysis (not reported here for space reason) to test the interaction among these factors but none of them is significant. The Tukey \ac{HSD} test  shows that all the browsing patterns are significantly different, being the \texttt{balanced} strategy the lowest one in terms of correlation and the \texttt{down} the highest one. The \ac{SOA} $\omega^2$ shows that the Browsing factor  is a large-size effect. Overall, this analysis suggests that the most prominent motivation of similarity between MsM measures and nEUs is a down browsing pattern.
 
%\begin{table}[tb]
%\centering
%\caption{ANOVA analysis of the factors influencing correlation with respect to nCTs.}
%\label{tab:anova_corr_nCTs}
%\small
%	\begin{tabular}{|l|r|r|r|r|r|r|}
%		\hline
%		\multicolumn{1}{|c}{\textbf{Source}} & \multicolumn{1}{|c}{\textbf{SS}} & \multicolumn{1}{|c}{\textbf{DF}} & \multicolumn{1}{|c}{\textbf{MS}} & \multicolumn{1}{|c}{\textbf{F}} & \multicolumn{1}{|c}{\textbf{p}} & \multicolumn{1}{|c|}{$\hat{\omega}_{\langle fact \rangle}^2$} \\
%		\hline
%		\textbf{Track}		& 0.2190	& 2		& 0.1095	& 7.5713	& 0.0014	& 0.1957 \\
%		\hline
%		\textbf{Patience}	& 0.3355	& 2		& 0.1678	& 11.6025	& < e-4		& 0.2820 \\
%        \hline
%		\textbf{Browsing}	& 0.2990	& 2		& 0.1495	& 10.3397	& 0.0001	& 0.2570 \\
%		\hline
%		\textbf{Weight}		& 0.0026	& 1		& 0.0026	& 0.1827	& 0.6710	& -- \\
%    	\hline
%		\textbf{Error}		& 0.6652	& 46	& 0.0145	& 			& 			& \\
%		\hline			
%		\textbf{Total}		& 1.5213	& 53	& 			& 			& 			& \\
%		\hline
%	\end{tabular}
%\end{table}    

%Table~\ref{tab:anova_corr_nCTs} shows the three-way \ac{ANOVA} for analysing the factors in the MsM measures which influence the correlation with nCTs.  The \ac{ANOVA} analysis shows that the Track (at a 5\% level but not at a 1\% one), Patience, and Browsing factors are statically significant while the Weight one is not; we also conducted an \ac{ANOVA} analysis (not reported here for space reason) to test the interaction among these factors but none of them is significant. The Tukey \ac{HSD} test shows that the \texttt{impatient} user is significantly different from the \texttt{balanced} and \texttt{patient} ones, which are not significantly different from each other, being the \texttt{patient} user the lowest one in terms of correlation and the \texttt{impatient} the highest one. The Tukey \ac{HSD} test also shows that the \texttt{down} browsing pattern is significantly different from the \texttt{balanced} and \texttt{reformulate} ones, which are not significantly different from each other, being the \texttt{down} strategy the lowest one in terms of correlation and the \texttt{balanced} the highest one. The \ac{SOA} $\omega^2$ shows that the Track, Patience and Browsing factors are large-size effects, being the user patience the most prominent one. Overall, even if the correlation is always extremely low, this analysis suggests that the most prominent motivations of similarity between MsM measures and nCTs are a balanced browsing pattern and an impatient user, which are the user model actually  implemented in nCTs which is extremely top-heavy.

Overall, the \ac{ANOVA} analysis also highlights that the Track factor, when significant, is not the most influencing one and this supports the previous observation about a consistent behaviour of the measures across the tracks and reporting the correlation values averages across tracks.


%\paragraph{Limitation of the Study:} 
%The correlation measures from the ST, shown in \ref{fig:kt_st}, gives a clue this data might not be useful for the analysis. Looking into the data, we find that the successive queries in sessions have large overlap of terms. This results in sessions returning mostly duplicate documents. Scoring measures are therefore largely determined given the retrieved document list for the first query of the session, which does not represent what we aim to measure. 
%
%Comparing the correlations with DDT, see \ref{fig:kt_ddt}, we see an indication that the longer session become, the stronger the correlation between sDCGs and EUs becomes, and the opposite goes for sDCGs and CTs.
%
%What also shows is that on ST, inner-correlations of MsM are very strong, and on DDT less. The lower values are caused by larger probabilities for jumping to the next query. As our focus lies on analysis of session performance metrics, for the remainder of the analysis we will focus solely on the DDT.


\begin{table}[tb]
\centering
\caption{\acf{DP} for different measures across different tracks. In bold, the maximum DP for both state-of-the-art and MsM measures.}
\label{tab:discriminative_power}
\small
\setlength{\tabcolsep}{1pt}
\hspace*{-1.5em}
\begin{tabular}{|l||r|*{2}{r|}|r|*{2}{r|}} 
\cline{1-4} 
\cline{1-4}
\multicolumn{1}{|c||}{\textbf{Measure}} & \multicolumn{1}{c|}{\textbf{DD2015}} & \multicolumn{1}{c|}{\textbf{DD2016}} & \multicolumn{1}{c||}{\textbf{DD2017}} \\ 
\cline{1-4} 
nsDCGs & 0.7532  & \textbf{0.8238} & \textbf{0.6727}    \\ 
\cline{1-4}
nEUs  & 0.7013  & 0.3667  & 0.5455 \\ 
\cline{1-4}
nCTs  & \textbf{0.8745}  & 0.6619  & 0.6364  \\ 
\hline 
\hline
				 & \multicolumn{3}{c||}{\textbf{Linear}} & \multicolumn{3}{c|}{\textbf{Log}} \\
\cline{2-7}
\multicolumn{1}{|c||}{\textbf{Measure}} & \multicolumn{1}{c|}{\textbf{DD2015}} & \multicolumn{1}{c|}{\textbf{DD2016}} & \multicolumn{1}{c||}{\textbf{DD2017}} & \multicolumn{1}{c|}{\textbf{DD2015}} & \multicolumn{1}{c|}{\textbf{DD2016}} & \multicolumn{1}{c|}{\textbf{DD2017}} \\ 
\hline
 MsM\_fwd\_imp\_down  & 0.8052  & 0.7190 & 0.4000 & 0.8095 & 0.6952 & 0.4727 \\  
\hline 
 MsM\_fwd\_imp\_bal  & 0.8225 & 0.7333 & 0.4364 & 0.7922 & 0.7810 & 0.4909 \\  
\hline 
 MsM\_fwd\_imp\_reform  & 0.8095 & 0.7857 & 0.4364 & 0.8442 & \textbf{0.8381} & 0.6000 \\ 
\hline 
MsM\_fwd\_bal\_down  & 0.8225 & 0.6810 & 0.4000  & 0.8312 & 0.7381 & 0.4909 \\
\hline 
MsM\_fwd\_bal\_bal  & 0.8225  & 0.8238 & 0.4727 & 0.8658 & 0.8238 & 0.5091\\ 
\hline 
MsM\_fwd\_bal\_reform  & \textbf{0.8701}  & 0.8143 & 0.4909 & 0.8442 & \textbf{0.8381} & 0.6000 \\  
\hline 
MsM\_fwd\_pat\_down  & 0.7706  & 0.7810 & 0.4545 & \textbf{0.8701} & 0.8238 & 0.6000 \\ 
\hline 
MsM\_fwd\_pat\_bal  & 0.8615 & 0.8286 & 0.5091 & 0.8312 & 0.8333 & \textbf{0.6364} \\ 
\hline 
MsM\_fwd\_pat\_reform  & 0.8615 & \textbf{0.8381} & \textbf{0.6000} & 0.8312 & 0.8238 & \textbf{0.6364} \\ 
\hline 
\hline 
\end{tabular}
\end{table}

\subsection{Discriminative Power}
\label{subsubsec:Discriminative_Power}

The \ac{DP} of the different measures is presented in Table~\ref{tab:discriminative_power}. What we observe is that the \ac{DP} of the proposed measures is on par with or sometimes improves with respect to the existing session search measures. 

If we consider the linear vs the logarithmic weighting function, the power of the MsM instantiations generally tends to increase when using the logarithmic weighting function and this is especially pronounced in the case of the DD2017 track. 

\begin{table}[tb]
\centering
\caption{ANOVA analysis of the factors influencing DP.}
\label{tab:anova_dp}
\small
	\begin{tabular}{|l|r|r|r|r|r|r|}
		\hline
		\multicolumn{1}{|c}{\textbf{Source}} & \multicolumn{1}{|c}{\textbf{SS}} & \multicolumn{1}{|c}{\textbf{DF}} & \multicolumn{1}{|c}{\textbf{MS}} & \multicolumn{1}{|c}{\textbf{F}} & \multicolumn{1}{|c}{\textbf{p}} & \multicolumn{1}{|c|}{$\hat{\omega}_{\langle fact \rangle}^2$} \\
		\hline
		\textbf{Track}		& 0.0457	& 2		& 0.0228	& 13.8528	& < e-4		& 0.3225 \\
		\hline
		\textbf{Patience}	& 0.0353	& 2		& 0.0176	& 10.7038	& 0.0001	& 0.2644 \\
        \hline
		\textbf{Browsing}	& 1.1143	& 2		& 0.5571	& 337.9550	& < e-4		& 0.9258 \\
		\hline
		\textbf{Weight}		& 0.0020	& 1		& 0.0020	& 1.2256	& 0.2740	& -- \\
    	\hline
		\textbf{Error}		& 0.0758	& 46	& 0.0016	& 			& 			& \\
		\hline			
		\textbf{Total}		& 1.2731	& 53	& 			& 			& 			& \\
		\hline
	\end{tabular}
\end{table}    

Table~\ref{tab:anova_dp} shows the three-way \ac{ANOVA} for analysing the factors in the MsM measures which influence the \ac{DP}. The \ac{ANOVA} analysis shows that the Track, Patience, and Browsing factors are statically significant while the Weight one is not; we also conducted an \ac{ANOVA} analysis (not reported here for space reason) to test the interaction among these factors but none of them is significant. The Tukey \ac{HSD} test shows that the \texttt{impatient} user is significantly different from the \texttt{balanced} and \texttt{patient} one, which are not significantly different from each other, being the \texttt{impatient} user the lowest one in terms of \ac{DP} and the \texttt{patient} the highest one. The Tukey \ac{HSD} test also shows that all the browsing patterns are significantly different from each other, being \texttt{down} the highest one in terms of \ac{DP} while \texttt{reformulate} is the lowest one. The \ac{SOA} $\omega^2$ shows that all the significant factors are large-size effects, being the browsing pattern the most prominent one, about three times bigger than the others.


% \begin{figure*}[tb]
% 	\begin{subfigure}{.5\textwidth}
% 		\centering
% 		\includegraphics[width=0.8\linewidth]{img/discriminativePower_DDT.pdf}
%     	\caption{Dynamic Domain track.}
%     	\label{fig:dp_ddt}
%     \end{subfigure}%
% \begin{subfigure}{.5\textwidth}
% \centering
% 		\includegraphics[width=0.8\linewidth]{img/discriminativePower_ST.pdf}
%     	\caption{Session track.}
%     	\label{fig:dp_st}
%     \end{subfigure}%
%     \caption{\acl{ASL} on the DDT and ST tracks.}
%     \label{fig:asl}
% \end{figure*}



% sDCG is unaffected by normalization and simplification. 
% Normalization improves DP of EU, simplification influence is small. For CT DP diminishes on normalization as well as on simplification.

% The linear Forwards variants of MsM all score higher than sDCG and nEU, CT scores are the highest.


% The logarithmic Forwards variants of MsM 

% MsM has good/the best Discriminative Power, and a reasonable $\Delta$. CT has good Discriminative Power, and a low $\Delta$. 

% MsM\_ld\_3a scores the highest, closely followed by MsM\_ld\_2b.
% % 0.8, 0.1, 0.05, 0.05  # 2b
% % 0.45, 0.0, 0.45, 0.1  # 3a

% Normalization has a large affect on the Delta



% General:
% What parameter values reflect what behavior/other metric? Infer from correlations?

% Dataset:
% - Why is there such a large difference between the datasets values? Are the values in ST bad, maybe because of a low number of different systems?
% - Why are the MsM correlations different between datasets?
% - On ST the correlation between MsM and CTs is huge, on DD close to none?
% - On EUs the other way around, though less strong.


% MsM inner correlation:
% - Why are all MsM variants so strongly correlated? Though less strong on DD.

% Metrics inter correlation:
% On DD all normalized versions are strongly correlated to non-normalized. On ST similar except for a low correlation of EUs and nEUs

% On DD EU and sDCG are strongly corr., not on ST. Other way around for sDCG and CT. On both sets EU-CT corr. is low.

% On DD MsM correlates to sDCG and EU, not to CT. On ST MsM correlates to sDCG and CT, not EU.


% MsM inter correlation on DD:
% - sDCG is strongly correlated on both sets. On DD it goes up for MsM3a/d/e/f/g, an down for 2b/3b. On ST down on 3c. It seems it is most effected by ‘r’.
% - EU seems to follow the same correlation. 
% - CT does the opposite.

% Explain…
% -Why is EUs-nEUS on ST so low, while normalization has an opposite effect on the other metrics? Why is the effect of normalization different for metrics?
% The range of EU is much larger and can be negative. 
% —— In the (original) code the gain gets divided by cutoff (max #queries), the CT over the the actual #queries. I took out the first division for now.


% -EU-sDCG between datasets?
% - CT-sDCG between datasets?
% Check relevance, maybe because list length is 5 vs 10?

% The low variance in ST might be caused by low variance of systems?

\if 0
\begin{figure*}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_2011_kendall.png}
  \caption{Session track 2011}
  \label{fig:kt_st_lin_2011}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_2012_kendall.png}
  \caption{Session track 2012}
  \label{fig:kt_st_lin_2012}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_2013_kendall.png}
  \caption{Session track 2013}
  \label{fig:kt_st_lin_2013}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_2014_kendall.png}
  \caption{Session track 2014}
  \label{fig:kt_st_lin_2014}
\end{subfigure}
\caption{Kendall's Tau correlation matrices showing correlation metrics measurements on runs for Session track, MsM linear variants.}
\label{fig:kt_ddt_lin}
\end{figure*}

\begin{figure*}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_log_2011_kendall.png}
  \caption{Session track 2011}
  \label{fig:kt_st_lin_2011}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_log_2012_kendall.png}
  \caption{Session track 2012}
  \label{fig:kt_st_lin_2012}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_log_2013_kendall.png}
  \caption{Session track 2013}
  \label{fig:kt_st_lin_2013}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_log_2014_kendall.png}
  \caption{Session track 2014}
  \label{fig:kt_st_lin_2014}
\end{subfigure}
\caption{Kendall's Tau correlation matrices showing correlation metrics measurements on runs for Session track, MsM logarithmic variants.}
\label{fig:kt_st_lin}
\end{figure*}
\fi



\if 0
\begin{figure*}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/dd_2015_kendall.png}
  \caption{Dynamic domain track 2015}
  \label{fig:kt_ddt_lin_2015}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/dd_2016_kendall.png}
  \caption{Dynamic domain track 2016}
  \label{fig:kt_ddt_lin_2016}
\end{subfigure}
\caption{Kendall's Tau correlation matrices showing correlation metrics measurements on runs for Dynamic Domain track, MsM linear variants.}
\label{fig:kt_ddt_lin}
\end{figure*}

\begin{figure*}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/dd_log_2015_kendall.png}
  \caption{Dynamic domain track 2015}
  \label{fig:kt_ddt_log_2015}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/dd_log_2016_kendall.png}
  \caption{Dynamic domain track 2016}
  \label{fig:kt_ddt_log_2016}
\end{subfigure}
\caption{Kendall's Tau correlation matrices showing correlation metrics measurements on runs for Dynamic Domain track, MsM logarithmic variants.}
\label{fig:kt_ddt_log}
\end{figure*}

\begin{figure*}[ht]
\centering
  \includegraphics[width=1\linewidth]{img/dd_norm_vs_simpl_kendall.png}
\caption{dd normal vs simplified metrics kendall. Not meant to be included as picture, but for now.}
\label{fig:dd_norm_vs_simpl_kendall}
\end{figure*}
\fi
