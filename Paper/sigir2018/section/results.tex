\section{Results and Analysis}
\label{subsec:Results_and_Analysis}
To answer \RQRef{1}, we conduct a correlation analysis using Kendall’s $\tau$~\cite{Kendall1948} among the rankings of systems produced by the different evaluation measures. \cite{Ferro2017} has shown that, even if the absolute correlation values are different, removing or not the lower quartile runs produces the same ranking of measures in terms of correlation; similarly, it was shown that both $\tau$ and AP correlation $\tau_{ap}$~\cite{YilmazEtAl2008} produce the same ranking of measures in terms of correlation. Therefore, we focus only on Kendall's $\tau$ without removing lower quartile systems. 

To answer \RQRef{2} we quantify the \acf{DP} of each measure using a paired bootstrap test~\cite{Sakai:2006}.

\subsection{Correlation Analysis}
\label{subsubsec:Correlation_Analysis}
\begin{figure*}[ht]
\centering
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/dd_kendall.png}
  \caption{Dynamic domain track}
  \label{fig:kt_ddt}
\end{subfigure}%
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_kendall.png}
  \caption{Session track}
  \label{fig:kt_st}
\end{subfigure}
\caption{Kendall's $\tau$ correlation matrices for different tracks, using $MsM$ reciprocal weights.}
\label{fig:Kendall}
\end{figure*}

\if 0
\begin{figure*}[ht]
\centering
\begin{subfigure}{.4\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/dd_log_kendall.png}
  \caption{Dynamic domain track}
  \label{fig:kt_ddt_log}
\end{subfigure}%
\begin{subfigure}{.4\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_log_kendall.png}
  \caption{Session track}
  \label{fig:kt_st_log}
\end{subfigure}
\caption{Kendall's $\tau$ correlation matrices for different tracks, using $MsM$ reciprocal logarithmic weights.}
\label{fig:Kendall_log}
\end{figure*}
\fi

%Figure~\ref{fig:Kendall} presents the Kendall's $\tau$ values across previous session search measures and different instantiations of the $MsM$ for DDT and ST.
  
Figure~\ref{fig:Kendall} presents the Kendall's $\tau$ correlation values for different measures across the two tracks.
 % 
On the DDT collection we make the following observations:
\begin{description}[topsep=0pt,nosep=0pt, leftmargin=0cm, noitemsep,nolistsep]
\item[1.] The correlations between simplified and original versions of existing metrics are all above 0.8 (not shown in the Figure due to space constraints), justifying comparisons between MsM and the simplified metrics.
\item[2.] The correlations between normalized and unnormalized versions of existing metrics, i.e. sDCG and nsDCG, EUs and nEUs, and CTs and nCTs are 0.9, 0.6, and 0.84 respectively (Figure~\ref{fig:kt_ddt}). The low correlation in the case of EU justifies the need for normalization proposed by ~\cite{Tang:2017}.
\item[3.] The simplified Cube Test appears to strongly disagree with all other measures with respect to system rankings. This is probably due to the way cost is calculated in CT, since it discounts the overall gain and not the gain per document (Figure~\ref{fig:kt_ddt}).
%\item[4.] sDCGs and EUs correlate stronger, even though the normalization of the two measures reduces their correlation to some extent (Figure~\ref{fig:kt_ddt}).
\item[4.] The different versions of MsM are all correlated with each other, to some extent (Figure~\ref{fig:kt_ddt}). In particular: 
\begin{description}[topsep=0pt,nosep=0pt, leftmargin=0cm, noitemsep,nolistsep]
\item[4a.] There is a strong correlation between all Forward models; this correlation appears to drop as the user is modeled to reformulate more (MsM-fwd-balanced and MsM-fwd-ranking vs. MsM-fwd-05-ref, MsM-fwd-03-ref+, and MsM-fwd-01-ref++) The Kendall's $\tau$ being the three reformulation-oriented models and MsM-fwd-balanced is 0.81, 0.79, and 0.77, while the Kendall's $\tau$ being the three reformulation-oriented models and MsM-fwd-ranking further drops. Based on these observations we conclude that the MsM measure can indeed accommodate different models of user: users that typically move forward and rarely reformulate, users that tend to abandon the session early, and users only scan the top documents and reformulate their queries.
\item[4b.] There is a strong correlation between all Random-walk models, i.e. between MsM-rd-balanced, MsM-rd-ranking and MsM-rd-ranking-back. Despite the different backwards probabilities between MsM-rd-ranking and MsM-rd-ranking-back we observe a rather strong correlation between the two measures, which is reasonable given that both measures, in their own way, put a lot of weight on the documents of the first ranked list.
\end{description}
\item[5.] The Forward models appear to have a mid-size correlation with nEUs, which drops as the forward probability increases in expense to the reformulation and stopping probabilities (MsM-fwd-balanced to MsM-fwd-ranking), while it increases to levels above 0.8 when the reformulation probability increases (MsM-fwd-03-ref+ to MsM-fwd-01-ref++).
% MsM-fwd-01-ref++ -> 0.79
%(Figure~\ref{fig:kt_ddt}.
\item[6.] The correlation with nEUs drops when the Random models are considered. The more time a user spends on a ranked list without reformulating (MsM-rd-ranking and MsM-rd-ranking-back) the smaller the correlation.
%\item[7.] The Cube Test appears not to correlate at all with the proposed measures. This can be due to the discount function used by CT.
\item[7.] When considering the reciprocal logarithmic weight of the MsM the correlations of the proposed measures with sDCG, nsDCGs, EUs, and nEUs, increases for all instantiations of it except for MsM-fwd-05-ref, MsM-fwd-03-ref+ and MsM-fwd-01-ref++ versus nEUS (not shown in the Figure due to space constraints).
\end{description}

While we leave the reduction of MsM to the existing measures as a future work, we can still make some sense of the existing measures in terms of their correlations with the different instantiations of MsM. Overall, it seems that sDCG, nsDCGs, EUs, and nEUs incorporate a user model in which reformulating queries is to a large extend not penalized much, given the strong correlation with MsM instantiations that model users who tend to reformulate and not spend a lot of time over documents in a ranked list.

When considering the ST dataset in Figure~\ref{fig:kt_st} we observe that (a) there is perfect correlation between all MsM instantiations, as well as the sDCG and CT variations, and (b) there is a near-zero correlation the EU variations. 
%
In order to better understand the differences between the correlations over the DDT and ST we look into the average relevant documents retrieved per query in the sessions as well as the median length of the sessions in the two tracks, both reported below.

\begin{tabular}{lcccccc}
\hline
        & Q1 & Q2 & Q3 & Q4 & Q5 & Q6 \\
DD2015 & 0.97 & 1.03 & 0.84 & 0.70 & 0.62 & 0.56 \\
DD2016 & 1.71 & 1.60 & 1.55 & - & - & - \\
ST2011 & 5.20 & 0.01 & 0.00 & - & - & - \\
ST2012 & 4.29 & 0.01 & - & - & - & - \\
ST2013 & 3.09 & 0.00 & 0.00 & 0.00 & - & - \\
ST2014 & 3.29 & 0.02 & 0.01 & 0.00 & - & - \\
\hline
\end{tabular}

What we observe is that not only the number of queries per session is smaller in ST than DDT, but also the percentage of relevant documents found in the ST beyond the first query is nearly zero. This first explains why different user models of across query dynamics in MsM do not change the ranking of systems. Further, the discount function of CT does not play an important role in ST since very few relevant documents are found and read in queries beyond the first one (and hence the good correlation with other measures, which is absent from DT). Last, a large number of relevant documents are returned in the first query of ST having as a result EU's gain function to dramatically penalize any follow up relevant documents, and hence the low correlation, which is not the case in DT and hence the high correlation there.
%To get a better understanding of this behavior we look into the correlations for each year of the ST separately (not presented due to space constraints). We observe that the strong correlations above hold for ST 2011 and 2012, while correlations differ for ST 2013 and 2014. A possible explanation of this is the difference in the session length across the different years. The longer the sessions the more the correlations approach the ones we observed in DDT. Another possible explanation is the small number of systems generated for ST (only 12 which actually belonged to a family of 4 methods with 3 variations each), which may make the results on ST unreliable.

%\paragraph{Limitation of the Study:} 
%The correlation measures from the ST, shown in \ref{fig:kt_st}, gives a clue this data might not be useful for the analysis. Looking into the data, we find that the successive queries in sessions have large overlap of terms. This results in sessions returning mostly duplicate documents. Scoring measures are therefore largely determined given the retrieved document list for the first query of the session, which does not represent what we aim to measure. 
%
%Comparing the correlations with DDT, see \ref{fig:kt_ddt}, we see an indication that the longer session become, the stronger the correlation between sDCGs and EUs becomes, and the opposite goes for sDCGs and CTs.
%
%What also shows is that on ST, inner-correlations of MsM are very strong, and on DDT less. The lower values are caused by larger probabilities for jumping to the next query. As our focus lies on analysis of session performance metrics, for the remainder of the analysis we will focus solely on the DDT.

\subsection{Discriminative Power}
\label{subsubsec:Discriminative_Power}

The \ac{DP} of the different measures is presented in Table~\ref{tab:discriminative_power}. What we observe is that the \ac{DP} of the proposed measures is on par with or improves with respect to the existing session search measures, while if we consider the logarithmic weighting function the power of the MsM instantiations tend to increase on the DDT track. In the ST track, the log-variants of MsM appear to be all same in \ac{DP} terms, equal to or smaller than to not-log counterparts. We hypothesize that this is a side-effect of smoothing the ``stochastic distances'' among documents when systems produce somehow close document rankings, as it may happen in ST where systems runs are variants of few core algorithms.

\begin{table}[]
\centering
\caption{\acf{DP} for different measures across different tracks. For $MsM$ measures both reciprocal (Rec) and reciprocal Logarithmic (Log) weights are shown.}
\label{tab:discriminative_power}
\begin{tabular}{l|ll|ll}
\hline
\multicolumn{1}{c|}{\textbf{Measure}} & \multicolumn{2}{c|}{\textbf{DDT}} & \multicolumn{2}{c}{\textbf{ST}} \\ % & \multicolumn{1}{l}{\textbf{ALL}} \\
 & Rec & Log & Rec & Log \\
\hline
sDCGs & \multicolumn{2}{c|}{0.7054}  & \multicolumn{2}{c}{0.8030}   \\
nsDCGs & \multicolumn{2}{c|}{0.7353} & \multicolumn{2}{c}{0.6818} \\
EUs & \multicolumn{2}{c|}{0.5991} & \multicolumn{2}{c}{0.7424}  \\
nEUs & \multicolumn{2}{c|}{0.7586}  & \multicolumn{2}{c}{0.5606}  \\
CTs & \multicolumn{2}{c|}{0.7630}  & \multicolumn{2}{c}{0.5606}   \\
nCTs & \multicolumn{2}{c|}{0.7685} & \multicolumn{2}{c}{0.5606}  \\ 
\hline
MsM-fwd-balanced 	& 0.7619 & 0.7841 & 0.8636 & 0.7424 \\
MsM-fwd-ranking 	& 0.7564 & 0.7896 & 0.7424 & 0.7424 \\ 
MsM-fwd-05-ref 		& 0.7519 & 0.6944 & 0.8636 & 0.7424 \\ 
MsM-fwd-03-ref+ 	& 0.7409 & 0.6966 & 0.8030 & 0.7424 \\ 
MsM-fwd-01-ref++ 	& 0.7309 & 0.6977 & 0.8030 & 0.7424 \\ 
MsM-rd-balanced 	& 0.7619 & 0.7852 & 0.8636 & 0.7424 \\ 
MsM-rd-ranking 		& 0.7453 & 0.7829 & 0.7424 & 0.7424 \\ 
MsM-rd-ranking-back	& 0.7575 & 0.7841 & 0.8636 & 0.7424 \\ 
\hline
\end{tabular}
\end{table}


\begin{figure*}[tb]
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.8\linewidth]{img/discriminativePower_DDT.pdf}
    	\caption{Dynamic Domain track.}
    	\label{fig:dp_ddt}
    \end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
		\includegraphics[width=0.8\linewidth]{img/discriminativePower_ST.pdf}
    	\caption{Session track.}
    	\label{fig:dp_st}
    \end{subfigure}%
    \caption{\acl{ASL} on the DDT and ST tracks.}
    \label{fig:asl}
\end{figure*}

Figure~\ref{fig:asl} shows the \ac{ASL} curves for various measures on the DDT and ST tracks. The \ac{ASL} is the ``proportion of statistically significant differences one can get out of a given experimental environment and therefore a measure of how reliable a metric is''~\cite{Sakai2012}. The more an \ac{ASL} curve is on the left, the higher is the \ac{DP} of a measure, since it corresponds to a larger number of system pairs achieving that value of \ac{ASL}. In the DDT track (Figure~\ref{fig:dp_ddt}), we can observe that nsDCGs has the smallest \ac{ASL}, all the MsM variants behave similarly to the traditional session-based measures, being the log-based ones slightly better. In the ST track (Figure~\ref{fig:dp_st}), we can note how the ASL curves are sharper, due to the fact that we have a smaller number of runs than in DDT, and that all the MsM variants outperform the traditional session-based measures. Moreover, we can also spot some finer-grained differences among the MsM log-variants, which are not actually all the same as it might have appeared from Table~\ref{tab:discriminative_power}. 


% sDCG is unaffected by normalization and simplification. 
% Normalization improves DP of EU, simplification influence is small. For CT DP diminishes on normalization as well as on simplification.

% The linear Forwards variants of MsM all score higher than sDCG and nEU, CT scores are the highest.


% The logarithmic Forwards variants of MsM 

% MsM has good/the best Discriminative Power, and a reasonable $\Delta$. CT has good Discriminative Power, and a low $\Delta$. 

% MsM\_ld\_3a scores the highest, closely followed by MsM\_ld\_2b.
% % 0.8, 0.1, 0.05, 0.05  # 2b
% % 0.45, 0.0, 0.45, 0.1  # 3a

% Normalization has a large affect on the Delta



% General:
% What parameter values reflect what behavior/other metric? Infer from correlations?

% Dataset:
% - Why is there such a large difference between the datasets values? Are the values in ST bad, maybe because of a low number of different systems?
% - Why are the MsM correlations different between datasets?
% - On ST the correlation between MsM and CTs is huge, on DD close to none?
% - On EUs the other way around, though less strong.


% MsM inner correlation:
% - Why are all MsM variants so strongly correlated? Though less strong on DD.

% Metrics inter correlation:
% On DD all normalized versions are strongly correlated to non-normalized. On ST similar except for a low correlation of EUs and nEUs

% On DD EU and sDCG are strongly corr., not on ST. Other way around for sDCG and CT. On both sets EU-CT corr. is low.

% On DD MsM correlates to sDCG and EU, not to CT. On ST MsM correlates to sDCG and CT, not EU.


% MsM inter correlation on DD:
% - sDCG is strongly correlated on both sets. On DD it goes up for MsM3a/d/e/f/g, an down for 2b/3b. On ST down on 3c. It seems it is most effected by ‘r’.
% - EU seems to follow the same correlation. 
% - CT does the opposite.

% Explain…
% -Why is EUs-nEUS on ST so low, while normalization has an opposite effect on the other metrics? Why is the effect of normalization different for metrics?
% The range of EU is much larger and can be negative. 
% —— In the (original) code the gain gets divided by cutoff (max #queries), the CT over the the actual #queries. I took out the first division for now.


% -EU-sDCG between datasets?
% - CT-sDCG between datasets?
% Check relevance, maybe because list length is 5 vs 10?

% The low variance in ST might be caused by low variance of systems?

\if 0
\begin{figure*}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_2011_kendall.png}
  \caption{Session track 2011}
  \label{fig:kt_st_lin_2011}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_2012_kendall.png}
  \caption{Session track 2012}
  \label{fig:kt_st_lin_2012}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_2013_kendall.png}
  \caption{Session track 2013}
  \label{fig:kt_st_lin_2013}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_2014_kendall.png}
  \caption{Session track 2014}
  \label{fig:kt_st_lin_2014}
\end{subfigure}
\caption{Kendall's Tau correlation matrices showing correlation metrics measurements on runs for Session track, MsM linear variants.}
\label{fig:kt_ddt_lin}
\end{figure*}

\begin{figure*}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_log_2011_kendall.png}
  \caption{Session track 2011}
  \label{fig:kt_st_lin_2011}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_log_2012_kendall.png}
  \caption{Session track 2012}
  \label{fig:kt_st_lin_2012}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_log_2013_kendall.png}
  \caption{Session track 2013}
  \label{fig:kt_st_lin_2013}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/st_log_2014_kendall.png}
  \caption{Session track 2014}
  \label{fig:kt_st_lin_2014}
\end{subfigure}
\caption{Kendall's Tau correlation matrices showing correlation metrics measurements on runs for Session track, MsM logarithmic variants.}
\label{fig:kt_st_lin}
\end{figure*}
\fi



\if 0
\begin{figure*}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/dd_2015_kendall.png}
  \caption{Dynamic domain track 2015}
  \label{fig:kt_ddt_lin_2015}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/dd_2016_kendall.png}
  \caption{Dynamic domain track 2016}
  \label{fig:kt_ddt_lin_2016}
\end{subfigure}
\caption{Kendall's Tau correlation matrices showing correlation metrics measurements on runs for Dynamic Domain track, MsM linear variants.}
\label{fig:kt_ddt_lin}
\end{figure*}

\begin{figure*}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/dd_log_2015_kendall.png}
  \caption{Dynamic domain track 2015}
  \label{fig:kt_ddt_log_2015}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{img/dd_log_2016_kendall.png}
  \caption{Dynamic domain track 2016}
  \label{fig:kt_ddt_log_2016}
\end{subfigure}
\caption{Kendall's Tau correlation matrices showing correlation metrics measurements on runs for Dynamic Domain track, MsM logarithmic variants.}
\label{fig:kt_ddt_log}
\end{figure*}

\begin{figure*}[ht]
\centering
  \includegraphics[width=1\linewidth]{img/dd_norm_vs_simpl_kendall.png}
\caption{dd normal vs simplified metrics kendall. Not meant to be included as picture, but for now.}
\label{fig:dd_norm_vs_simpl_kendall}
\end{figure*}
\fi
