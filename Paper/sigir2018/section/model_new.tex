\section{The Model}
\label{sec:TheModel}


\subsection{Multi-query session dynamics}

% We stem from the notation proposed by~\cite{FerranteEtAl2015b} 
% for defining the basic concepts of topics, documents, 
% ground-truth, run and judged run and we extend it to account for a multi session scenario.

% Let us consider a set of {\bf documents} $D$ and a set of {\bf topics} $T$. 
% Let ($REL$, $\preceq$) be a totally ordered set of \textbf{relevance degrees}, 
% where we assume the existence of a minimum that we 
% call the \textbf{non-relevant} relevance degree $\text{\tt nr} = \min(REL)$.  
% We assume that $REL$  is a finite set of cardinality 3 and
% we denote its strictly ordered 
% elements as $rel_0, rel_1$ and $rel_2$, where 
% $rel_0 = \text{\tt nr}$. The reason for this choice will be clear in the sequel, when we will
% divide the relevant documents in two classes: those which are relevant and have been 
% already visited by a user ($rel_1$) and those relevant and not seen before ($rel_2$).

% For each pair $(t,d)\in T\times D$, the {\bf ground-truth}  $GT$ is a map which assigns a  
% relevance degree $rel \in REL$ to a document $d$ with respect to a topic $t$. The {\bf recall base} 
% is the map $RB$ from $T$ into $\N$ defined as the total number of relevant  documents for a given 
% topic $t \mapsto RB_t= \big| \{d\in D: GT(t,d)\succ \text{\tt nr}\} \big|$. 

% Given a run $r_t=(d_{1},\dots,d_{N})$ of length $N$, let $\hat r_t[i]$ be the relevance assigned to the 
% document $d_{i}$ for the topic $t$, i.e. $\hat r_t[i]=GT(t,d_{i})$. 

% Given a positive integer $N,$ the length of the run, we define the {\bf set of retrieved documents} as 
% $D(N)=\{(d_1,\ldots, d_N): d_i\in D, d_i\neq d_j \ \mbox{for any} \ i\neq j\}$,  i.e.~the ranked list of 
% retrieved documents without duplicates, and the {\bf universe set of retrieved documents}  as 
% ${\cal D}:=\bigcup_{N=1}^{|D|} D(N)$.

For a given task, a user can generate a sequence of queries, each of which originates a ranked list of documents. Called $D$ the whole document corpus and $N \in \N$ the length of a run, $D_j(N)=\{(d_{1,j},\ldots, d_{N,j}): d_{n,j}\in D, d_{n,j}\neq d_{m,j} \ \mbox{for any} \ m\neq n\}$  is the ordered set of documents retrieved by a system run for the $j$-th query. The sets $D_j(N)$ for $j\in \N$ may not be disjoint and the same document may appear in many queries. Without loss of generality we assume that any run has the same length $N$.

Let $k\in \N$ the number of queries in a session. The whole search session is defined as a matrix of documents, where columns are the the runs $D_1(N), \ldots, D_k(N)$ corresponding to each query
\[
\left[
\begin{array}{ccccc}
d_{1,1} & d_{1,2} & d_{1,3} & \ldots & d_{1,k} \\
d_{2,1} & d_{2,2} & d_{2,3} & \ldots & d_{2,k} \\
d_{3,1} & d_{3,2} & d_{3,3} & \ldots & d_{3,k} \\
\vdots & \vdots & \vdots & \ddots & \vdots  \\
%\vdots & \vdots & \vdots & \vdots & \vdots  \\
d_{N,1} & d_{N,2} & d_{N,3} & \ldots & d_{N,k} 
\end{array}
\right]
\]
 
The user moves among the documents according to some dynamics, that for simplicity we assume to be Markovian. This means that the user decides which document to visit only on the basis of the last document considered. Moreover, we assume that the user starts her search from the first document in the first query, i.e. first row and first column. Then, she moves among the documents in the first column until she decides to change column, i.e. to reformulate the query, or to abandon the search session.  In case of query reformulation, she passes to the first document of the subsequent column, i.e. the next query, and so on until she ends the search.

We define the sequence of positions of the documents visited by the user as a stochastic process, i.e. a sequence of random variables, $(X_n=(X^1_n,X^2_n))_{n\ge 1}$, where $X_n=(i,j)$ means that the $n$-th document visited by the user is $d_{i,j}$, the $i$-th document of the $j$-th column. We assume that this process is a Markov chain on the state space  $S=\{1,\ldots,N\}^\infty\cup \{(F,j), j\in \N\}$ where $X_n=(F,j)$ represents the fact that the user ends his search after visiting $n-1$ documents and formulating $j$ queries. %Note that in principle we don't know how many queries will be performed by the user and so the number $k$ of these runs can grow indefinitely.
%
The transition matrix of this Markov chain, i.e. the values of all the 
conditional probabilities 
\[
p_{(i_n,j_n),(i_{n+1},j_{n+1})}=\G[X_{n+1}=(i_{n+1},j_{n+1})|X_{n}=(i_{n},j_{n})] \ ,
\]
undergo these constraints:
\begin{enumerate}
\item
$p_{(i_n,j_n),(i_{n+1},j_{n+1})}=0 \ $ if $\ j_{n+1}\neq j_{n},j_n+1$, i.e.
the user  can either move within a column of documents or pass to the next one;
\item
$p_{(i_n,j_n),(i_{n+1},j_{n}+1)}=0 \ $ if $\ i_{n+1}\neq 1$, i.e. when the user leaves a column, she goes to the first document of the next one;
\item
$p_{(i_n,j_n),(F,j_{n})}>0 \ $ for any $\ i_{n}\neq F$;
\item
$p_{(F,j_n),(F,j_{n})}=1 \ $ for any $\ j_{n}$, i.e. the states $(F,j)$'s are all \emph{absorbing}.
\end{enumerate}
\begin{example}
Let us assume that the stochastic process $(X_n)_{n\ge 1}$ takes the following values:
\[
\begin{array}{l}
X_1=(1,1), X_2=(3,1), X_3=(1,2), X_4=(2,2), X_5=(6,2), 
\\
X_6=(3,2), X_7=(1,3), X_8=(3,3), X_9=(F,3).
\end{array}
\]
This means that the user performed $3$ queries and considered 8 documents before stopping, as shown in the following graph
\begin{center}
\begin{tikzpicture}[->, >=stealth', auto, semithick, node distance=1.5cm]
\tikzstyle{every state}=[fill=white,draw=black,thick,text=black,scale=1]
\node    (A)                     {$(1,1)$};
\node    (AA)[below of=A] {$$};
\node    (B)[right of=A]   {$(1,2)$};
\node    (C)[right of=B]   {$(1,3)$};
\node    (D)[below of=AA]   {$(3,1)$};
\node    (E)[below of=B]        {$(2,2)$};
\node    (EE)[below of=E]        {$(3,2)$};
\node    (F)[below of=EE]   {$(6,2)$};
\node    (FF)[below of=C]        {$$};
\node    (G)[below of=FF]   {$(3,3)$};
\node    (HH)[below of=G]        {$$};
\node    (H)[below of=HH]   {$({\color{red}\bf{F}},3)$};
\path
(A) edge    (D)
%    edge[bend left]     node{$(1-p)^2$}     (B)
%    edge[bend left,below]      node{$p(1-p)$}      (D)
%    edge[bend right]    node{$p(1-p)$}      (C)
(D) edge           (B)
(B) edge        (E)
(E) edge[bend right,right] (F)
(F) edge (EE)
(EE) edge (C)
(C) edge (G)
(G) edge (H);
%    edge[bend right,right]    node{$q(1-q)$}      (B)
%    edge[bend left]     node{$q(1-q)$}      (C)
%    edge[bend left,above]     node{$q^2$}         (A);
%\node[above=0.5cm] (A){Patch G};
\end{tikzpicture}
\end{center}
\end{example}
In order to determine how many queries have been issued and how long did the search last,
we define the following sequence of stopping times. Recall that a \emph{stopping
time} for a Markov chain $(X_n)_{n\ge 1}$ is a random variable $T$ with values in $\N\cup\{\infty\}$
such that for any $n\in \N$ the event $\{T=n\}$ depends only on $\{X_m, m\le n\}$.
The stopping time 
\[
H=\inf\{n\ge 1: X^1_n=F\}
\]
determines the number of steps done by the process, with the convention that $\inf \emptyset = \infty$. It allows us to define the (random) number $K$ of queries performed during the search
\[
K=X^2_H 
\]
i.e. $K$ is the second component of the process $(X_n,n\in \N)$ once absorbed in $(F,\cdot)$.

Then, we define the random times to leave any query as
\[
\begin{array}{l}
H^1:=\inf\{n\ge 1: X^2_n=2\}
\\
H^2:=\inf\{n\ge 1: X^2_n=3\}
\\
\ \ \ \ \ \ \vdots
\\
H^{K-1}:=\inf\{n\ge 1: X^2_n=K-1\}
\end{array}
\]
Thanks to these stopping times, we are able to determine how many documents of any query have been visited by a user. Indeed, defined $H^0=1$, the user has considered $H^1-H^0$ documents of the first query,  $H^2-H^1$ documents of the second query, $H^3-H^2$ documents of the third query and so on until  the last query, where the number of documents visited is $H-H^{K-1}$. In the previous example, we have $H=9$, $K=3$, $H^1=3$, $H^2=7$, and the user has visited, respectively, 2, 4 and 2 documents in the three queries before stopping the search.

By means of these stopping times, we can define the events corresponding to the end of  the search session in any given query. Indeed, if $H^1=\infty$, it means that the user never passes to the second query and since $(F,1)$ is the unique absorbing state, $A^1=\{\omega: H^1(\omega)=\infty\}$ corresponds to the event ``the user visits just the documents in the first query''. Analogously, for any $j>1$, we can define the event 
$A^j=\{\omega: H^1(\omega)<\infty, \ldots, H^{j-1}(\omega)<\infty, H^{j}(\omega)=\infty\}$ that the user ends is search after considering the first $j$ queries. 
The events  $\{A^j, j \in \N\}$ form a partition of $\Omega$, i.e. are pairwise disjoint and their union coincides with $\Omega$.

In the following, these events are used to measure how ``often'' a random user  visits each query during her search and to obtain, as a consequence,  a weight to be assigned to any query. Moreover, to evaluate the effectiveness of the search within the queries actually visited by the user, we evaluate how (stochastically) far is any state, i.e. any document of any query, form the initial state $(1,1)$ and discount its relevance proportionally to this ``random'' distance.
%by defining these new families of stopping times.
%Let us consider a generic query $k$ and assume that
%\begin{equation}
%\label{query-k}
%p_{(i,k),(1,k+1)}=0 \ , \ \forall \ 1\le i \le N
%\end{equation}
%i.e. we can no more leave the $k$-th query.
%Let us assume that the Markov chain $(X_n,n\ge 1)$ starts now from $(1,k)$
%and for any $i\in\{1,\ldots,N\}$ let us define 
%\[
%H^k(i)=\inf\{n\ge 1: X_n=i\} .
%\]
%These stopping times allow us to say how much does it takes to reach the document at depth $i$, and 
%these values will be used in the following subsection to evaluate the average inside the columns.

\subsection{Evaluation of Multi-query Sessions}

%Given the previous definition of random relevance, we can turn every binary evaluation measure into a random evaluation measure, simply composing the original expression of each measure with the random relevances. 

% When we consider a single query, an \emph{evaluation measure} is an application 
% \[
% M:T\times D \rightarrow \R_+
% \]
% obtained by the composition of the judged run with
% the map 
% \[
% \mu:{\cal R} \rightarrow \R_+
% \]
% giving $M=\mu\big(GT(t, d_1), \ldots, GT(t, d_N)\big)$.

In the case of single query search, an evaluation measure is often defined as a discounted average of the relevance at each rank position (see e.g. nDCG), where the discount is a some increasing function of the rank position. In order to adopt this simple and successful approach in our stochastic multi-query setting, we replace these deterministic discounts operating a two-step stochastic procedure: 
\begin{itemize}
\item
Given that the search generates $k$ queries, 
we consider the probabilities $h_1,h_2, \ldots, h_k$ 
that the search ends in $(F,1)$, $(F,2),$ $\ldots, (F,k)$, respectively;
\item
% Given that the user ends her search after the state $(F,j-1)$, i.e. that she visits the
% document in the $j$-th query, we compute the discount at each rank position of the $j$ queries according to the expected number of steps needed to reach this rank
% position starting form $(1,1)$.
% Given that the user ends her search in the state $(F,k)$, i.e. she visits the documents $k$ queries, we compute the discount at each rank position of each of the visited queries according to the expected number of steps needed to reach that rank position starting form $(1,1)$.
Given that the user does not end her search before the query $j$, i.e. she visits the documents of the query $j$, 
we compute the discount at each rank position of the $j$-th query according to the expected number of steps needed to reach that rank position starting form $(1,1)$.
\end{itemize}

Assume from now on that the Markov process starts from the state $(1,1)$ and to have $k$  queries.  All the probabilities and expectations have to be considered as conditioned given the event $D=\{X_1=(1,1),K=k\}$, even if we omit this in the notation.

The user can stop her search after considering only the first run, or the first two runs, or the first three runs and so on. This is equivalent to considering that the Markov chain is absorbed in $(F,1)$, or $(F,2)$ and so on until $(F,k)$.
We are able to evaluate the absorption probabilities in any of these states $h=(h^1,\ldots,h^k)$ starting form the probabilities of the events $A^1, \ldots, A^{k-1}$ defined above.  Indeed, we have $h^j=\G[A^j]$ for any $j<k$, and $h^k=1-h^1-\ldots-h^{k-1}$.
%
%transition matrix. We denote by
%\[
%H^{F,j}=\inf\{n\ge1: X_n=(F,j)\}
%\]
%with the convention that $\inf \emptyset = \infty$. Taking
%\[
%h^{F,j} = \G[H^{F,j}<\infty|X_1=(1,1)]
%\]
%we get that the $h^{F,j}$'s represent, for any $j$, the absorption probability into the absorbing state $(F,j)$.

Let us define $\pi_j$ as the probability that the user visits the query $j$ 
before ending the search
\[
\pi_{j}=\sum_{l=j}^k h^{l} = 1 - \sum_{l=1}^{j-1} h^{l} \ .
\]
% These values do not sum 1, but may represent the proportion of
% times that a user indeed visit a given query.
% So we propose to normalize these values to 1 and to 
% use these new coefficients to assign the weights to the score of
% any single query.
% To make an example, if $k=4$ and $h^1=0.4, h^2=0, h^3=0.4, h^4=0.2$,
% we get $\pi^1=1, \pi^2=0.6, \pi^3=0.6$ and $\pi^4=0.2$.
% This means, that once normalized to 1, the coefficients become
% $5/12, 3/12, 3/12, 1/12$. 
% As a second example, assume that
% \[
% p_{(i,j),(i+1,j)}=1 \ , \ \forall \ 1\le i \le N-1 \ \mbox{and} \ j\le k
% \]
% and
% \[
% p_{(N,j),(F,j)}=0 \ , \ \forall \ 1\le j < k \ .
% \]
% We get that $\pi^{j}=1$ for any $j\le k$,
% since the user visits all the documents in any query
% with probability $1$.
% Indeed, if we proceed always to the following document and are absorbed in the last state $(F,k)$,
% then $h^{F,j}=0$ for $j<k$ and $h^{F,k}=1$.
% Then, 
% $\pi^{F,j}=1$ for any $j\le k$ and $\pi^F=k$.
%define a new multi-session measure 
%taking the expectation of the previous values $E(j)$ according to this distribution.
%More precisely, we define
%\[
%\pi^F=\sum_{j=1}^k \pi^{F,j} = \sum_{j=1}^k (k-j) h^{F,j} \ .
%\]
%and

%Let us now define the discount to apply to the relevance of any single document in a run.
% by considering a single query, with the assumption on the transition probabilities (\ref{query-k}).
%One of the most successful measure in IR is nDCG, which is basically a discounted  sum of the relevance present at each position of the run, where the discount is a function of the rank position, $\max\{1,\log_b(\cdot)\}$ for nDCG. In order to take into account the stochastic model we have described above, we replace the rank position $n$ with its ``expected distance'' form the state $(1,1)$.

To evaluate the ``expected distance'' from state $(1, 1)$ for the documents in query $j$, 
we define the following family of stopping times for any $i\le N$, since the search does not end before this query $j$
\[
H^{(i,j)}=\inf\{n\ge 1: X_n=(i,j)\} \, .
\]
These stopping times allow us to evaluate how long does it takes to reach the document at depth $i$ in query $j$, and these values are used to perform the average inside the columns.

Thus, given that the search does not end before document $(i,j)$, we define the weight at position $(i,j)$ as
\begin{equation}
e(i,j)=\E_{(1,1)}[H^{(i,j)}]=\E[H^{(i,j)}|X_1=(1,1)] \ .
\label{eq:eij}
\end{equation}
% \textcolor{red}{Note that assuming that
% \begin{equation}
% \label{trivial_case}
% p_{(i,j),(i+1,j)}=1 \ , \ \forall \ 1\le i \le N
% \end{equation}
% with the convention that $(N+1,k)=(F,k)$, we get
% $e(i,k)=i$ for any $i\le N$.}
To evaluate the contribution of the $j$-th query to the multi-session search, 
we compute
\begin{equation}
\label{E(j)}
E(j)=\sum_{i=1}^N \phi(e(i,j)) \ GT(d_{i, j})
\end{equation}
where $GT(d_{i,j}) \in \N_0$ is the gain corresponding to document $d_{i,j}$ ($0$ for not relevant documents) and the discount function $\phi$ is a positive, non-increasing real function. In the following, we experiment two choices of the function $\phi$:
\begin{itemize}
\item
\emph{Reciprocal weight}, i.e. $\phi(x)=\frac{1}{x}$;
\item
\emph{Reciprocal logarithmic weight}, i.e. $\phi(x)=\frac{1}{1 + \log_{10}(x)}$;
% \item
% \emph{Exponential weight}, i.e. $\phi(x)=exp(-x)$.
\end{itemize}

%\textcolor{red}{Note that under (\ref{trivial_case}) and assuming the Reciprocal logarithmic weight,
%the measure (\ref{E(j)}) becomes the standard DCG measure.}

Finally, the new  \emph{Markov Session Measure} ($MsM$) combines the contribution of the $k$ queries in a search session as
\begin{equation}
\label{MsM}
MsM= \sum_{j=1}^k \pi_{j} E(j) \ .
\end{equation}
% where
% \[
% \pi=\sum_{j=1}^k \pi^{j} = \sum_{j=1}^k (k-j) h^{j} \ .
% \]

\subsection{User's dynamic examples}

As an exemplification, in this paper, we consider two user's models, with two different set of assumptions. 

The first one, called {\bf Forward model}, is inspired by the \ac{RBP} philosophy.
We assume that the user moves forward in the ranked list and after considering a document
she decides, according to constant probabilities, to proceed to the next document, to stop her search, or to reformulate a new query. 
From the transition matrix point of view, this model is determined by the following additional assumptions:
% \begin{equation}
% \label{ForwardModel}
% \begin{array}{l}
% p_{(i,j),(i+1,j)}=p \ , \ \ p_{(i,j),(1,j+1)}=r \ \mbox{if} \ \ i\neq F,N , \ j<k
% \\
% p_{(i,j),(F,j)}=s \ \ \mbox{if} \ \ i\neq F , \ j<k
% \\
% p_{(N,j),(1,j+1)}=r/(1-p)=r_N \ \ \mbox{if} \ \ i\neq F , \ j<k
% \\
% p_{(N,j),(F,j)}=s/(1-p)=s_N \ \ \mbox{if} \ \ i\neq F , \ j<k
% \\
% p_{(i,k),(i+1,k)}=p/(1-r)=\overline{p} \ \ \mbox{if} \ \ i\neq F
% \\
% p_{(i,k),(F,k)}=s/(1-r)=\overline{s} \ \ \mbox{if} \ \ i\neq F \ ,
% % \\
% % p_{(F,j),(F,j)}=1 \ \ \mbox{for any} \ \ j \ ,
% \end{array}
% \end{equation}
\begin{equation*}
\label{ForwardModel}
\begin{array}{l}
p_{(i,j),(i+1,j)}=p \ \  \mbox{if} \ \ i\neq F
\\
p_{(i,j),(1,j+1)}=r \ \ \mbox{if} \ \ i\neq F
\\
p_{(i,j),(F,j)}=s \ \ \mbox{if} \ \ i\neq F
\\
p_{(F,j),(F,j)}=1 \ \ \mbox{for any} \ \ j \ ,
\end{array}
\end{equation*}
% \begin{equation}
% \label{ForwardModel}
% \begin{array}{l}
% p_{(i_n,j_n),(i_{n}+1,j_{n})}=p_{nd} \ \  \mbox{if} \ \ i_n\neq F
% \\
% p_{(i_n,j_n),(1,j_{n}+1)}=p_{nq} \ \ \mbox{if} \ \ i_n\neq F
% \\
% p_{(i_n,j_n),(F,j_{n})}=p_{end} \ \ \mbox{if} \ \ i_n\neq F
% \\
% p_{(F,j_n),(F,j_{n})}=1 \ \ \mbox{for any} \ \ j_{n} \ ,
% \end{array}
% \end{equation}
where $p+r+s=1$ and $p>0$, $r>0$, and $s>0$.
% and $nq$ stands for {\em next query}, 
% $nd$ stands for {\em next document} and $end$ for end of the search.

\begin{example}
Let us consider $N=3$, $k=3$. The corresponding forward model 
is represented below
\begin{center}
\begin{tikzpicture}[->, >=stealth', auto, semithick, node distance=1.5cm]
\tikzstyle{every state}=[fill=white,draw=black,thick,text=black,scale=1]
\node    (A)                     {$(1,1)$};
\node (Z)[right of=A] {$ $};
\node    (AA)[below of=A] {$(2,1)$};
\node    (AAA)[below of=D]   {$(\bf{F},1)$};
\node    (B)[right of=Z]   {$(1,2)$};
\node (ZZ)[right of=B] {$ $};
\node    (C)[right of=ZZ]   {$(1,3)$};
\node    (D)[below of=AA]   {$(3,1)$};
\node    (E)[below of=B]        {$(2,2)$};
\node    (EE)[below of=E]        {$(3,2)$};
\node    (F)[below of=EE]   {$(\bf{F},2)$};
\node    (FF)[below of=C]        {$(2,3)$};
\node    (G)[below of=FF]   {$(3,3)$};
%\node    (HH)[right of=B]        {$$};
\node    (H)[below of=G]   {$(\bf{F},3)$};
\path
(A) edge  node{$p$}  (AA)
(A) edge[bend right=40] node[left] {$s$} (AAA)
(AA) edge[bend right=30] node[left] {} (AAA)
(A) edge  node{$r$}  (B)
(AA) edge  node{} (B)
(AA) edge    node{}  (D)
(D) edge   node{} (AAA)
%    edge[bend left]     node{$(1-p)^2$}     (B)
%    edge[bend left,below]      node{$p(1-p)$}      (D)
%    edge[bend right]    node{$p(1-p)$}      (C)
(D) edge     node{}     (B)
(B) edge[bend right=40] node[left] {} (F)
(E) edge[bend right=30] node[left] {} (F)
(B) edge    node{}    (C)
(B) edge    node{}    (E)
(E) edge   node{}     (C)
(EE) edge    node{}    (C)
(E) edge node{} (EE)
(EE) edge node{} (F)
%(EE) edge (C)
%(C) edge[bend right=40] node[left] {$\overline{s}$} (H)
(FF) edge[bend right=30] node[left] {} (H)
(C) edge node{} (FF)
(FF) edge node{} (G)
(G) edge node{} (H);
%    edge[bend right,right]    node{$q(1-q)$}      (B)
%    edge[bend left]     node{$q(1-q)$}      (C)
%    edge[bend left,above]     node{$q^2$}         (A);
%\node[above=0.5cm] (A){Patch G};
\end{tikzpicture}
\end{center}
\end{example}

The second model, called {\bf Random-Walk model}, generalizes the previous one by allowing the user to move forward or backward in the ranked list. Indeed, after considering a document we assume that she decides, with constant probabilities, to proceed to the next document, to the previous document,  to stop the search or to reformulate a new query. 
% From the transition matrix point of view, this model is determined by the following additional assumptions:
The transition matrix becomes
\begin{equation*}
\label{RWModel}
\begin{array}{l}
p_{(i,j),(i+1,j)}=p \ \ \mbox{if} \ \ i\neq F
\\
\textcolor{red}{p_{(i,j),(i-1,j)}=q \ \ \mbox{if} \ \ i\neq 1}
\\
p_{(i,j),(1,j+1)}=r \ \ \mbox{if} \ \ i\neq F
\\
p_{(i,j),(F,j)}=s \ \ \mbox{if} \ \ i\neq F
\\
p_{(F,j),(F,j)}=1 \ \ \mbox{for any} \ \ j
\end{array}
\end{equation*}
% \begin{equation}
% \label{RWModel}
% \begin{array}{l}
% p_{(i,j),(i+1,j)}=p \ \ \mbox{if} \ \ i\neq F
% \\
% \textcolor{red}{p_{(i,j),(i-1,j)}=q \ \ \mbox{if} \ \ i\neq 1}
% \\
% p_{(i,j),(1,j+1)}=r \ \ \mbox{if} \ \ i\neq F
% \\
% p_{(i,j),(F,j)}=s \ \ \mbox{if} \ \ i\neq F
% \\
% p_{(F,j),(F,j)}=1 \ \ \mbox{for any} \ \ j
% \end{array}
% \end{equation}
% \begin{equation}
% \label{RWModel}
% \begin{array}{l}
% p_{(i_n,j_n),(i_{n}+1,j_{n})}=p_{nd} \ \ \mbox{if} \ \ i_n\neq F
% \\
% p_{(i_n,j_n),(i_{n}-1,j_{n})}=p_{pd} \ \ \mbox{if} \ \ i_n\neq 1
% \\
% p_{(i_n,j_n),(1,j_{n}+1)}=p_{nq} \ \ \mbox{if} \ \ i_n\neq F
% \\
% p_{(i_n,j_n),(F,j_{n})}=p_{end} \ \ \mbox{if} \ \ i_n\neq F
% \\
% p_{(F,j_n),(F,j_{n})}=1 \ \ \mbox{for any} \ \ j_{n}
% \end{array}
% \end{equation}
where $p+q+r+s=1$ and $p>0$, $q\ge0$, $r>0$ and $s>0$.

\begin{example}
Let us consider again $N=3$, $k=3$. The corresponding random-walk model is represented below
\begin{center}
\begin{tikzpicture}[->, >=stealth', auto, semithick, node distance=1.5cm]
\tikzstyle{every state}=[fill=white,draw=black,thick,text=black,scale=1]
\node    (A)                     {$(1,1)$};
\node (Z)[right of=A] {$ $};
\node    (AA)[below of=A] {$(2,1)$};
\node    (AAA)[below of=D]   {$(\bf{F},1)$};
\node    (B)[right of=Z]   {$(1,2)$};
\node (ZZ)[right of=B] {$ $};
\node    (C)[right of=ZZ]   {$(1,3)$};
\node    (D)[below of=AA]   {$(3,1)$};
\node    (E)[below of=B]        {$(2,2)$};
\node    (EE)[below of=E]        {$(3,2)$};
\node    (F)[below of=EE]   {$(\bf{F},2)$};
\node    (FF)[below of=C]        {$(2,3)$};
\node    (G)[below of=FF]   {$(3,3)$};
%\node    (HH)[right of=B]        {$$};
\node    (H)[below of=G]   {$(\bf{F},3)$};
\path
(A) edge[bend right]  node{$p$}  (AA)
(A) edge[bend right=40] node[left] {$s$} (AAA)
(AA) edge[bend right=30] node[left] {} (AAA)
(AA) edge[bend right,red]  node[right] {$q$}  (A)
(A) edge  node{$r$}  (B)
(AA) edge[bend right]   node{}  (D)
(D) edge[bend right,red]  node[right] {}  (AA)
(AA) edge  node{}   (B)
(D) edge node{} (AAA)
(D) edge      node{}     (B)
(B) edge    node{}    (C)
(B) edge[bend right]   node{}     (E)
(B) edge[bend right=40] node[left] {}(F)
(E) edge[bend right=30] node[left] {}(F)
(E) edge[bend right, red]   node[right] {}    (B)
(E) edge     node{}   (C)
(EE) edge   node{}     (C)
(E) edge[bend right] node{} (EE)
(EE) edge[bend right,red]  node[right] {} (E)
(EE) edge node{} (F)
(EE) edge (C)
(C) edge[bend right] node{} (FF)
(FF) edge[bend right] node{} (G)
(FF) edge[bend right,red] node[right] {} (C)
(C) edge[bend right=40] node[left] {}(H)
(FF) edge[bend right=30] node[left] {}(H)
(G) edge[bend right,red] node[right] {} (FF)
(G) edge node{} (H);
%    edge[bend right,right]    node{$q(1-q)$}      (B)
%    edge[bend left]     node{$q(1-q)$}      (C)
%    edge[bend left,above]     node{$q^2$}         (A);
%\node[above=0.5cm] (A){Patch G};
\end{tikzpicture}
\end{center}
\end{example}

For both models, the transition probabilities have to be rescaled on the boundary  
such as, for example, the last document of the last query for which there is no forward probability $p$. Details are provided in algorithm~\ref{MsM-pseudo-code} in the next section.


% \TODO{We have not experimented with probabilities conditioned on relevance.}
% To obtain a more realistic model, we can assume that the previous probabilities
% depend on the relevance of the documents in the ranked list. This approach is in the spirit of \ac{ERR}
% and should provide a better description of the user behaviour.
% In this case, we assume that the previous transition probabilities $p_{(i_n,j_n),(\cdot,\cdot)}$
% depend on the relevance of the document in position $(i_n,j_n)$.
% More precisely, we assume that
% \[
% p_{(i_n,j_n),(i_{n}+1,j_{n})}=p_{nd}(\hat r_t[i_n,j_n]) 
% \]
% and similarly for all the other probabilities in (\ref{ForwardModel}) and (\ref{RWModel}).
% In this case instead of 2, respectively 3, parameters, we will have
% 6 and 9 parameters, since we have to define the values of $p_{nd}(rel_i)$,
% $p_{pd}(rel_i)$, $p_{nq}(rel_i)$, for $i=0, 1, 2$.


\subsection{Computation of the $MsM$ measure}

Algorithm~\ref{MsM-pseudo-code} provides the pseudo-code for computing the $MsM$ measure in a simplified and efficient way. The algorithm stems from the observation that, since the random variables in eq.~\ref{eq:eij} can take the value $\infty$ with positive probability, their expectation is equal to $\infty$. Nevertheless, in order to measure the ``stochastic distance'' of the document at position $(i,j)$ from that in $(1,1)$, we can compute the expectation under the restriction that the search does not end before reaching the target document. In practice, we can equal to $0$ the probability to  reach the state $F$ from the states above $(i,j)$ and normalize the other elements to still have a stochastic matrix. Therefore, in the algorithm, we can use a single transition matrix $P$, representing the dynamic within a generic query, with $N+2$ states: the first $N$ states represents the $N$ retrieved documents within the query, the $F = N+1$ state is the end of the search session, and the $Q = N+2$ is the reformulation to the next query. We then, appropriately, select rows and columns from this transition matrix $P$, rescale them into a $\hat P$ matrix ensuring it is stochastic, and perform the different computations, as discussed in the pseudo-code.

\begin{algorithm*}
	\KwIn{$run$ a $N \times K$ integer matrix where rows are documents, columns are queries, and each cell contains a natural number representing the relevance of a document, $0$ for not relevant. $i$ is the document index, $j$ is the query index.}
    \KwIn{$p$, $q$, $r$, and $s$, the transition probabilities of, respectively: moving forward to the next document within a query; moving backward to the previous document within a query; moving to the next query, i.e. jumping to the first document of the next query; and, stopping a search session. 
    We assume $p+q+r+s=1$, $p>0$, $q\geq 0$, $r>0$, and $s>0$.}
    
    \BlankLine
    \KwOut{the $MsM$ measure for the input $run$.}
    
    \BlankLine\BlankLine
    
    \KwData{For the first document of a generic query, there is no backward transition probability, so we have to rescale the transition probabilities:
    $
    	p_1 = \frac{p}{p+s+r}, \; s_1 = \frac{s}{p+s+r}, \; r_1 = \frac{r}{p+s+r}
    $}
    \KwData{For the last document of a generic query, there is no forward transition probability, so we have to rescale the transition probabilities:
    $
    	q_N = \frac{q}{q+s+r}, \; s_N = \frac{s}{q+s+r}, \; r_N = \frac{r}{q+s+r}
    $}
    \KwData{The transition matrix $P$ of a generic query is a 
    $(N+2) \times (N+2)$ stochastic matrix where:
    \begin{itemize}
    	\item the leading $N \times N$ principal sub-matrix contains the backward and forward transitions between the documents within a query, paying attention to the re-scaled transition probabilities for the first and last document;
        \item the additional absorbing state $F=N+1$ represents the end of a search session;
        \item the additional absorbing state $Q=N+2$ represents the jump to the next query.
    \end{itemize}
    \begin{equation*}
    	\hspace{3em}P =
        \kbordermatrix{
				& 1			& 2			& 3		& 0			& \cdots	& N-2	& N-1	& N		& \vrule	& F		& Q \\
			1	& 0			& p_1		& 0		& 0			& \cdots	& 0		& 0		& 0		& \vrule	& s_1	& r_1 \\
            2	& q			& 0			& p 	& 0			& \cdots	& 0		& 0		& 0		& \vrule	& s		& r \\
            3	& 0			& q			& 0 	& p			& \cdots	& 0		& 0		& 0		& \vrule	& s		& r \\
       \vdots	& \vdots	& \vdots	&\vdots	& \vdots	& \ddots	& \vdots& \vdots&\vdots	&  \vrule	& \vdots& \vdots \\
            N-1	& 0			& 0			& 0 	& 0			& \cdots	& q		& 0		& p		& \vrule	& s		& r \\
            N	& 0			& 0			& 0 	& 0			& \cdots	& 0		& q_N	& 0		& \vrule	& s_N	& r_N \\
            \cline{2-12}
            F	& 0			& 0			& 0 	& 0			& \cdots	& 0		& 0		& 0		& \vrule	& 1		& 0 \\
            Q	& 0			& 0			& 0 	& 0			& \cdots	& 0		& 0		& 0		& \vrule	& 0		& 1 \\
		}        
    \end{equation*}}
    \KwData{$e$ is a $N \times 1$ vector, representing the average time to go from document $1$ to documents $1, \ldots, N$, respectively.}
    \KwData{$e_Q$ is a scalar, representing the average time to move to the next query.}
    \KwData{$h_F$ is a scalar, representing the probability to go from document $1$ to state $F$, i.e. end of search session, in the first $K-1$ queries.}
    \KwData{$h_{FK}$ is a scalar, representing the probability to go from document $1$ to state $F$, i.e. end of search session, in the $K$-th query.}
    \KwData{$h$ is a $1 \times K$ vector, representing the probability to go from document 1 of the first query to the state $(F,1), \ldots, (F,K)$, respectively.} 
    %of the $j$-th query.}
    \KwData{$\pi$ is a $1 \times K$ vector, representing the probability of ending the search session in query $1, \ldots, K$, respectively.}
    \KwData{$g$ is a $1 \times K$ vector, representing the total gain of each query.}
    
    \BlankLine\BlankLine    
    \tcc{the average time to go from document 1 to itself is 0}
    $e(1) \leftarrow 0$\;
    
    \BlankLine    
    \tcc{$\hat P_1$ is the same matrix as $P$ but where the $F$ and $Q$ state rows and columns have been removed since, to compute the average time to move from document 1 to document $i$, you must assume that neither the search session has ended nor you have moved to the next query}
    $\hat P_1 \leftarrow P(1:N, 1:N)$\;
    ensure $\hat P_1$ is a stochastic matrix, i.e. normalize each row so that it sums up to 1\;
    
    \BlankLine    
    \For{$i \leftarrow 1$ \KwTo $N-1$}{
        	
        \tcc{compute the average time to go from each document to document $i+1$ ($\mathbf{I}_i$ is the $i \times i$ identity matrix)}
        $tmp \leftarrow \Big(\mathbf{I}_i - \hat P_1(1:i, 1:i)\Big)^{-1}
         \begin{pmatrix}1 \\ 1 \\ \vdots \\ 1\end{pmatrix}$\;
                 
        \tcc{ we are interested only in the average time to go from document 1 to document $i+1$}
        $e(i+1) \leftarrow tmp(1)$\;
    }
            
	\caption{Pseudo-code for computing the $MsM$ measure in eq.~\eqref{MsM}. (It continues on the next page.)\label{MsM-pseudo-code}}
\end{algorithm*}

\setcounter{algocf}{0}
\begin{algorithm*}

	\tcc{$\hat P_2$ is the same matrix as $P$ but where the $F$ state rows and columns have been removed since, to compute the average time to move to the next query, you must assume that the search session has not ended}
    $\hat P_2 \leftarrow P( [1:N \,\, Q], [1:N \,\, Q])$\;
    ensure $\hat P_2$ is a stochastic matrix, i.e. normalize each row so that it sums up to 1\;
    
    \BlankLine
    \tcc{compute the average time to go from each document to the next query}
    $tmp \leftarrow \Big(\mathbf{I}_N - \hat P_1(1:N, 1:N)\Big)^{-1}
         \begin{pmatrix}1 \\ 1 \\ \vdots \\ 1\end{pmatrix}$\;
    \tcc{we are interested only in the average time to go from document 1 to the next query}
	$e_Q \leftarrow tmp(1)$\;
    
    \BlankLine\BlankLine
    \tcc{compute the probability to go from document $i$ to state $F$ in the first $K-1$ queries}
    $tmp \leftarrow \Big(\mathbf{I}_N - P(1:N, 1:N)\Big)^{-1}
         P(1:N, F)$\;
	\tcc{we are interested only in the probability to go from document $1$ to state $F$ in the first $K-1$ queries}
	$h_F \leftarrow tmp(1)$\;         
         
	\BlankLine\BlankLine
    \tcc{$\hat P_3$ is the same matrix as $P$ but where the $Q$ state rows and columns have been removed since, to compute the probability to go from document $i$ to state $F$ in the $K$-th query, there is not next query}
    $\hat P_3 \leftarrow P(1:F, 1:F)$\;
    ensure $\hat P_3$ is a stochastic matrix, i.e. normalize each row so that it sums up to 1\;

    \BlankLine
    \tcc{compute the probability to go from document $i$ to state $F$ in the $K$-th query}
    $tmp \leftarrow \Big(\mathbf{I}_N - \hat P_3(1:N, 1:N)\Big)^{-1}
         \hat P_3(1:N, F)$\;
	\tcc{we are interested only in the probability to go from document $1$ to state $F$ in the $K$-th query}
	$h_{FK} \leftarrow tmp(1)$\;           

	\BlankLine\BlankLine
    \tcc{compute the probability to go from document $1$ (of the first query) to state $F$ of the $j$-th query}
    \For{$j \leftarrow 1$ \KwTo $K-1$}{
    	$h(j) \leftarrow (1 - h_F)^{j-1} \cdot h_F$\;
	}
    $h(K) \leftarrow (1 - h_F)^{K-1} \cdot h_{FK}$\;
    
    \BlankLine\BlankLine
    \tcc{compute the probability of ending the search session in query $j$}
    $\pi(1) \leftarrow 1$\;
    \For{$j \leftarrow 2$ \KwTo $K$}{
    	$\pi(j) \leftarrow 1 - \sum_{l=1}^{j-1} h(l) $\;
    }
    
    \BlankLine\BlankLine
    \tcc{discount each retrieved document by the average time to get to it}
    \For{$i \leftarrow 1$ \KwTo $N$}{
    	\For{$j \leftarrow 1$ \KwTo $K$}{
        	$tmp(i, j) \leftarrow \frac{run(i,j)}{1 + e(i) + e_Q}$\;
        
     \BlankLine
     \tcc{In case of logarithmic discount, use instead  
   	      $tmp(i, j) \leftarrow \frac{run(i,j)}{1 + \log_{10}(1 + e(i) + e_Q)}$\;
          }
    	}
    }
    
    \BlankLine\BlankLine
    \tcc{compute the total gain for each query}
    \For{$j \leftarrow 1$ \KwTo $K$}{
        	$g(j) \leftarrow \pi(j) \sum_{i=1}^N e(i)$\;
    }
    
    \BlankLine\BlankLine
    \tcc{compute the $MsM$ measure}
    $MsM \leftarrow \sum_{j=1}^K g(j)$\;
    
	\caption{Pseudo-code for computing the $MsM$ measure in eq.~\eqref{MsM}. (Continued from the previous page.)}
\end{algorithm*}




