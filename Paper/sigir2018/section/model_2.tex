\section{The Model}
\label{sec:TheModel}


\subsection{Multi-query session dynamics}

We stem from the notation proposed by~\cite{FerranteEtAl2015b} for defining the basic concepts of topics, documents, 
ground-truth, run and judged run and we extend it to account for a multi session scenario.

Let us consider a set of {\bf documents} $D$ and a set of {\bf topics} $T$. 
Let ($REL$, $\preceq$) be a totally ordered set of \textbf{relevance degrees}, 
where we assume the existence of a minimum that we 
call the \textbf{non-relevant} relevance degree $\text{\tt nr} = \min(REL)$.  
We assume that $REL$  is a finite set of cardinality 3 and
we denote its strictly ordered 
elements as $rel_0, rel_1$ and $rel_2$, where 
$rel_0 = \text{\tt nr}$. The reason for this choice will be clear in the sequel, when we will
divide the relevant documents in two classes: those which are relevant and have been 
already visited by a user ($rel_1$) and those relevant and not seen before ($rel_2$).

For each pair $(t,d)\in T\times D$, the {\bf ground-truth}  $GT$ is a map which assigns a  
relevance degree $rel \in REL$ to a document $d$ with respect to a topic $t$. The {\bf recall base} 
is the map $RB$ from $T$ into $\N$ defined as the total number of relevant  documents for a given 
topic $t \mapsto RB_t= \big| \{d\in D: GT(t,d)\succ \text{\tt nr}\} \big|$. 

Given a run $r_t=(d_{1},\dots,d_{N})$ of length $N$, let $\hat r_t[i]$ be the relevance assigned to the 
document $d_{i}$ for the topic $t$, i.e. $\hat r_t[i]=GT(t,d_{i})$. 

Given a positive integer $N,$ the length of the run, we define the {\bf set of retrieved documents} as 
$D(N)=\{(d_1,\ldots, d_N): d_i\in D, d_i\neq d_j \ \mbox{for any} \ i\neq j\}$,  i.e.~the ranked list of 
retrieved documents without duplicates, and the {\bf universe set of retrieved documents}  as 
${\cal D}:=\bigcup_{N=1}^{|D|} D(N)$.

In this paper we assume that a user for a given task can generate a sequence of queries. Any query
give rise to a run of documents and we will denote by $D_i(N)=\{(d_{1,i},\ldots, d_{N,i}): d_{n,i}\in D, 
d_{n,i}\neq d_{m,i} \ \mbox{for any} \ m\neq n\}$ the order set of documents retrieved by a system given
the i-th query. We will assume moreover that the sets $D_i(N)$ for $i\in \N$ may not be disjoint and
that the same document could appear in many queries. Without loss of generality we assume that
any run has the same length.

Given $k\in \N$, we can define a matrix of documents taking the runs $D_1(N), \ldots, D_k(N)$
has its columns
\[
\left[
\begin{array}{ccccc}
d_{1,1} & d_{1,2} & d_{1,3} & \ldots & d_{1,k} \\
d_{2,1} & d_{2,2} & d_{2,3} & \ldots & d_{2,k} \\
d_{3,1} & d_{3,2} & d_{3,3} & \ldots & d_{3,k} \\
\vdots & \vdots & \vdots & \ddots & \vdots  \\
%\vdots & \vdots & \vdots & \vdots & \vdots  \\
d_{N,1} & d_{N,2} & d_{N,3} & \ldots & d_{N,k} 
\end{array}
\right]
\]
 
Let us now assume that a user is considering these documents. 
He moves among the documents according to a given dynamics, that for simplicity
we will assume Markovian. This means that she/he will decide which document to visit
only on the bases of the previous document considered and its relevance.
Moreover we assume that the user starts is search by the first document in the first
column, then she/he can moves among the documents in the first column until she/he decides
to change column. In this case she/he passes to the column subsequent column and its
first document, and so on until he stops is search.

In order to formalise this description, we define the sequence of positions of the documents
visited by the user by a stochastic process, i.e. a sequence of
random variables, $(X_n=(X^1_n,X^2_n))_{n\ge 1}$, where
$X_n=(i,j)$ means that the $n$-th document visited by the
user is $d_{i,j}$, the $i$-th document of the $j$-th column.
We will assume in the sequel that this process will be a Markov chain on the state space 
$S=\{1,\ldots,N\}^\infty\cup \{(F,j), i\in \N\}$ where $X_n=(F,j)$ represents the fact that the user has stopped is search
after visiting $n-1$ documents and formulating $j$ different queries.
Note that in principle we don't know how many queries will be performed by the user and so the number $k$
of these runs can grow indefinitely.

The transition matrix of this Markov chain, i.e. the values of all the possible
conditional probabilities 
\[
p_{(i_n,j_n),(i_{n+1},j_{n+1})}=\G[X_{n+1}=(i_{n+1},j_{n+1})|X_{n}=(i_{n},j_{n})] \ ,
\]
undergo the following constraints:
\begin{enumerate}
\item
$p_{(i_n,j_n),(i_{n+1},j_{n+1})}=0 \ $ if $\ j_{n+1}\neq j_{n},j_n+1$ (we can move along the single column of documents or
pass to the successive one);
\item
$p_{(i_n,j_n),(i_{n+1},j_{n}+1)}=0 \ $ if $\ i_{n+1}\neq 1$ (when we leave a column, we go to the first document of
the successive one);
\item
$p_{(i_n,j_n),(F,j_{n})}>0 \ $ for any $\ i_{n}\neq F$;
\item
$p_{(F,j_n),(F,j_{n})}=1 \ $ for any $\ j_{n}$ (the $(F,j)$ are all absorbing states).
\end{enumerate}
\begin{example}
Let us assume that the stochastic process $(X_n)_n$ takes the following values:
\[
\begin{array}{l}
X_1=(1,1), X_2=(3,1), X_3=(1,2), X_4=(2,2), X_5=(6,2), 
\\
X_6=(3,2), X_7=(1,3), X_8=(3,3), X_9=(F,3).
\end{array}
\]
This means that the user performed $5$ queries and considered 9 documents before stopping.
The sequence of the documents visited can be described with the following graph
\begin{center}
\begin{tikzpicture}[->, >=stealth', auto, semithick, node distance=1.5cm]
\tikzstyle{every state}=[fill=white,draw=black,thick,text=black,scale=1]
\node    (A)                     {$(1,1)$};
\node    (AA)[below of=A] {$$};
\node    (B)[right of=A]   {$(1,2)$};
\node    (C)[right of=B]   {$(1,3)$};
\node    (D)[below of=AA]   {$(3,1)$};
\node    (E)[below of=B]        {$(2,2)$};
\node    (EE)[below of=E]        {$(3,2)$};
\node    (F)[below of=EE]   {$(6,2)$};
\node    (FF)[below of=C]        {$$};
\node    (G)[below of=FF]   {$(3,3)$};
%\node    (HH)[right of=B]        {$$};
\node    (H)[below of=G]   {$(\bf{F},3)$};
\path
(A) edge    (D)
%    edge[bend left]     node{$(1-p)^2$}     (B)
%    edge[bend left,below]      node{$p(1-p)$}      (D)
%    edge[bend right]    node{$p(1-p)$}      (C)
(D) edge           (B)
(B) edge        (E)
(E) edge[bend right,right] (F)
(F) edge (EE)
(EE) edge (C)
(C) edge (G)
(G) edge (H);
%    edge[bend right,right]    node{$q(1-q)$}      (B)
%    edge[bend left]     node{$q(1-q)$}      (C)
%    edge[bend left,above]     node{$q^2$}         (A);
%\node[above=0.5cm] (A){Patch G};
\end{tikzpicture}
\end{center}
\end{example}
In order to determine how many queries have been performed and how long does the search last,
we will define the following sequence of \emph{stopping times}. Recall that a stopping
time for a Markov chain is a random variable $T$ defined on $\N\cup\{\infty\}$
such that the event $\{T=n\}$ depends only on $\{X_m, m\le n\}$ for any $n\in \N$.
The first stopping time will determine the number of steps done by the process
\[
H=\inf\{n\ge 1: X^1_n=F\}.
\]
and allows us to define the (random) number $K$ of queries performed during the search
\[
K=X^2_H 
\]
i.e. the second component of the process $(X_n,n\in \N)$ once absorbed in $(F,\cdot)$.
Then we will define
the random times when we leave any column of documents as
\[
\begin{array}{l}
H^1:=\inf\{n\ge 1: X^2_n=2\}
\\
H^2:=\inf\{n\ge 1: X^2_n=3\}
\\
\ \ \ \ \ \ \vdots
\\
H^{K-1}:=\inf\{n\ge 1: X^2_n=K-1\}
\end{array}
\]
Thanks to these stopping times, we are able to determine how many documents of any
single query have been considered by the (random) user.
Indeed, defined $H^0=1$, the user has considered $H^1-H^0$ documents of the first query, 
$H^2-H^1$ documents of the second query, $H^3-H^2$ documents of the third query and so on until 
the last query, where the number of documents visited is $H-H^{K-1}$.
Coming back to the previous example, we have $H=9$, $K=3$, $H^1=3$, $H^2=7$ and the
user has visited, respectively, two, four and two documents in the three queries before stopping is
search.

This family of stopping times is used in the following section to measure how long the user will
stay on each single query during his search and to give the relative weight of the single queries.
On the contrary, to evaluate the effectiveness of the search inside a single query we proceed by
defining these new families of stopping times.
Let us consider a generic query $k$ and assume that
\begin{equation}
\label{query-k}
p_{(i,k),(1,k+1)}=0 \ , \ \forall \ 1\le i \le N
\end{equation}
i.e. we can no more leave the $k$-th query.
Let us assume that the Markov chain $(X_n,n\ge 1)$ starts now from $(1,k)$
and for any $i\in\{1,\ldots,N\}$ let us define 
\[
H^k(i)=\inf\{n\ge 1: X_n=i\} .
\]
These stopping times allow us to say how much does it takes to reach the document at depth $i$, and 
these values will be used in the following subsection to make the average inside the columns.

\subsection{Evaluation of Multi-query Sessions}

%Given the previous definition of random relevance, we can turn every binary evaluation measure into a random evaluation measure, simply composing the original expression of each measure with the random relevances. 

When we consider a single query, a \emph{evaluation measure} is an application 
\[
M:T\times D \rightarrow \R_+
\]
obtained by the composition of the judged run with
the map 
\[
\mu:{\cal R} \rightarrow \R_+
\]
giving $M=\mu\big(GT(t, d_1), \ldots, GT(t, d_N)\big)$.
The function $\mu$ is often defined as a discounted average of the
relevance present at each rank position (see e.g. nDCG), where the
discount is a suitable increasing function of the rank position.
In order to follow this simple and successful approach in the multi-session setting, we 
replace the discounts in two ways:
\begin{enumerate}
\item
inside the single query $k$ we compute the discount at each rank position $i$
proportionally to the inverse of the expectation of $H^k(i)$;
\item
then we consider the expectations of the stopping times $H^1$, $H^2, \ldots$ and 
average the previous values according to these weights.
\end{enumerate}


Let us start by considering a single query, with the assumption on the transition probabilities (\ref{query-k}).
One of the most successful measure in IR is nDCG, which is simply a discounted 
sum of the relevance present at each position of the run.
The discount is a function of the rank position ($\log_2$ for nDCG) and in order
to take into account the stochastic model we consider before, we replace
the rank position $i$ with the conditional expectation of the stopping time $H^k(i)$
when the process start from the first position in the rank.
More precisely, we define the weight at position $(i,k)$ as
\[
e(i,k)=\E_{(1,k)}[H^k(i)]=\E[H^k(i)|X_1=(1,k)] \ .
\]
To evaluate the contribution of the $q$-th query to the multi-session search, 
we will evaluate
\[
E(k)=\sum_{i=1}^N \phi(e(i,k)) \ \hat r_t[i,k]
\]
where
$\hat r_t[i,k]=GT(t,d_{i,k})$ and $\phi$ is a positive, non-increasing real function.

To take into account the contribution of the multi-sessions, we
can proceed with two different strategies.
\begin{enumerate}
\item
The first one takes into account the stopping times $H^k$, i.e. the random
time needed by the user in order to arrive to the $k$-th query.
Proceeding as before, we consider the original Markov chain and evaluate
\[
ms(k)=\E_{(1,1)}[H^k]=\E[H^k|X_1=(1,1)] \ .
\]
Thanks to these values, we define the Multi-session Measure (MsM) as
\[
MsM_1=\sum_{k=1}^\infty \phi(ms(k)) E(k) \ .
\]
In the sequel we test our $MsM$ measure for two choices of the function $\phi$:
\begin{enumerate}
\item
{\bf Reciprocal weight}, i.e. $\phi(x)=1/x$;
\item
{\bf Exponential weight}, i.e. $\phi(x)=exp(-x)$.
\end{enumerate}
\item
Given that a single search lasts for a random time $K=k$, we consider the
$k$ ranked lists of documents obtained in response of the $k$ different queries.
We will assume now that a user visits these documents with the dynamics described
before, modified as follows:
\begin{enumerate}
\item
$p_{(i_n,j_n),(F,j_{n})}=0 \ $ for any $\ i_{n}\neq F$;
\item
$p_{(i_n,j_n),(1,j_{n}+1)}=const\neq 0 \ $ for any $\ i_{n}\neq F$, with the convention that $k+1=1$.
\end{enumerate}
In this way we have an irreducible Markov chains and we can compute its invariant distribution 
$\pi=(\pi_1,\ldots, \pi_k)$,
which can be interpreted as a measure of how often we user is visiting any query on the long time. 
Thus we use this weights to add the contribution to the measure of any given query as
\[
MsM_2=\sum_{i=1}^k \pi_i E(i) \ .
\]



\end{enumerate}



\subsection{User's dynamic}

In this paper we consider two user's models, with two different set of assumptions. 
The first one, called {\bf Forward model}, is inspired by the \ac{RBP} philosophy.
In this case we assume that the user moves forward in the ranked list and after considering a document
she/he decides, according to constant probabilities, to proceed to the successive document, to stop his search or
to reformulate a new query. 
From the transition matrix point of view, this model is determined by the following additional assumptions:
\begin{equation}
\label{ForwardModel}
\begin{array}{l}
p_{(i_n,j_n),(i_{n}+1,j_{n})}=p_{nd} \ \  \mbox{if} \ \ i_n\neq F
\\
p_{(i_n,j_n),(1,j_{n}+1)}=p_{nq} \ \ \mbox{if} \ \ i_n\neq F
\\
p_{(i_n,j_n),(F,j_{n})}=p_{end} \ \ \mbox{if} \ \ i_n\neq F
\\
p_{(F,j_n),(F,j_{n})}=1 \ \ \mbox{for any} \ \ j_{n} \ ,
\end{array}
\end{equation}
where $p_{nq}+p_{nd}+p_{end}=1$ and $nq$ stands for {\em next query}, 
$nd$ stands for {\em next document} and $end$ for end of the search.

\begin{example}
Let us consider $N=3$, $k=3$. We get
\begin{center}
\begin{tikzpicture}[->, >=stealth', auto, semithick, node distance=1.5cm]
\tikzstyle{every state}=[fill=white,draw=black,thick,text=black,scale=1]
\node    (A)                     {$(1,1)$};
\node (Z)[right of=A] {$ $};
\node    (AA)[below of=A] {$(2,1)$};
\node    (AAA)[below of=D]   {$(\bf{F},1)$};
\node    (B)[right of=Z]   {$(1,2)$};
\node (ZZ)[right of=B] {$ $};
\node    (C)[right of=ZZ]   {$(1,3)$};
\node    (D)[below of=AA]   {$(3,1)$};
\node    (E)[below of=B]        {$(2,2)$};
\node    (EE)[below of=E]        {$(3,2)$};
\node    (F)[below of=EE]   {$(\bf{F},2)$};
\node    (FF)[below of=C]        {$(2,3)$};
\node    (G)[below of=FF]   {$(3,3)$};
%\node    (HH)[right of=B]        {$$};
\node    (H)[below of=G]   {$(\bf{F},3)$};
\path
(A) edge    (AA)
(A) edge[bend right] (AAA)
(AA) edge[bend right] (AAA)
(A) edge    (B)
(AA) edge    (B)
(AA) edge    (D)
(D) edge (AAA)
%    edge[bend left]     node{$(1-p)^2$}     (B)
%    edge[bend left,below]      node{$p(1-p)$}      (D)
%    edge[bend right]    node{$p(1-p)$}      (C)
(D) edge          (B)
(B) edge[bend right] (F)
(E) edge[bend right] (F)
(B) edge        (C)
(B) edge        (E)
(E) edge        (C)
(EE) edge        (C)
(E) edge (EE)
(EE) edge (F)
(EE) edge (C)
(C) edge[bend right] (H)
(FF) edge[bend right] (H)
(C) edge (FF)
(FF) edge (G)
(G) edge (H);
%    edge[bend right,right]    node{$q(1-q)$}      (B)
%    edge[bend left]     node{$q(1-q)$}      (C)
%    edge[bend left,above]     node{$q^2$}         (A);
%\node[above=0.5cm] (A){Patch G};
\end{tikzpicture}
\end{center}
\end{example}

The second model, called {\bf Random-Walk model}, generalizes the previous model
allowing that the user could move forward or backward in the ranked list. Indeed, after considering a document
we assume that he decides, with constant probabilities, to proceed to the successive document, to the previous document, 
to stop his search or to reformulate a new query. 
From the transition matrix point of view, this model is determined by the following additional assumptions:
\begin{equation}
\label{RWModel}
\begin{array}{l}
p_{(i_n,j_n),(i_{n}+1,j_{n})}=p_{nd} \ \ \mbox{if} \ \ i_n\neq F
\\
p_{(i_n,j_n),(i_{n}-1,j_{n})}=p_{pd} \ \ \mbox{if} \ \ i_n\neq 1
\\
p_{(i_n,j_n),(1,j_{n}+1)}=p_{nq} \ \ \mbox{if} \ \ i_n\neq F
\\
p_{(i_n,j_n),(F,j_{n})}=p_{end} \ \ \mbox{if} \ \ i_n\neq F
\\
p_{(F,j_n),(F,j_{n})}=1 \ \ \mbox{for any} \ \ j_{n}
\end{array}
\end{equation}
where $p_{nq}+p_{pd}+p_{nd}+p_{end}=1$ and $pd$ stands for {\em previous document}.

\begin{example}
Let us consider again $N=3$, $k=3$. We get
\begin{center}
\begin{tikzpicture}[->, >=stealth', auto, semithick, node distance=1.5cm]
\tikzstyle{every state}=[fill=white,draw=black,thick,text=black,scale=1]
\node    (A)                     {$(1,1)$};
\node (Z)[right of=A] {$ $};
\node    (AA)[below of=A] {$(2,1)$};
\node    (AAA)[below of=D]   {$(\bf{F},1)$};
\node    (B)[right of=Z]   {$(1,2)$};
\node (ZZ)[right of=B] {$ $};
\node    (C)[right of=ZZ]   {$(1,3)$};
\node    (D)[below of=AA]   {$(3,1)$};
\node    (E)[below of=B]        {$(2,2)$};
\node    (EE)[below of=E]        {$(3,2)$};
\node    (F)[below of=EE]   {$(\bf{F},2)$};
\node    (FF)[below of=C]        {$(2,3)$};
\node    (G)[below of=FF]   {$(3,3)$};
%\node    (HH)[right of=B]        {$$};
\node    (H)[below of=G]   {$(\bf{F},3)$};
\path
(A) edge[bend right]    (AA)
(A) edge[bend right] (AAA)
(AA) edge[bend right] (AAA)
(AA) edge[bend right,red]    (A)
(A) edge    (B)
(AA) edge[bend right]     (D)
(D) edge[bend right,red]    (AA)
(AA) edge     (B)
(D) edge (AAA)
%    edge[bend left]     node{$(1-p)^2$}     (B)
%    edge[bend left,below]      node{$p(1-p)$}      (D)
%    edge[bend right]    node{$p(1-p)$}      (C)
(D) edge           (B)
(B) edge        (C)
(B) edge[bend right]        (E)
(B) edge[bend right] (F)
(E) edge[bend right] (F)
(E) edge[bend right, red]       (B)
(E) edge        (C)
(EE) edge        (C)
(E) edge[bend right] (EE)
(EE) edge[bend right,red] (E)
(EE) edge (F)
(EE) edge (C)
(C) edge[bend right] (FF)
(FF) edge[bend right] (G)
(FF) edge[bend right,red] (C)
(C) edge[bend right] (H)
(FF) edge[bend right] (H)
(G) edge[bend right,red] (FF)
(G) edge (H);
%    edge[bend right,right]    node{$q(1-q)$}      (B)
%    edge[bend left]     node{$q(1-q)$}      (C)
%    edge[bend left,above]     node{$q^2$}         (A);
%\node[above=0.5cm] (A){Patch G};
\end{tikzpicture}
\end{center}
\end{example}




To obtain a more realistic model, we can assume that the previous probabilities
depend on the relevance of the documents in the ranked list. This approach is in the spirit of \ac{ERR}
and should provide a better description of the user behaviour.
In this case, we assume that the previous transition probabilities $p_{(i_n,j_n),(\cdot,\cdot)}$
depend on the relevance of the document in position $(i_n,j_n)$.
More precisely, we assume that
\[
p_{(i_n,j_n),(i_{n}+1,j_{n})}=p_{nd}(\hat r_t[i_n,j_n]) 
\]
and similarly for all the other probabilities in (\ref{ForwardModel}) and (\ref{RWModel}).
In this case instead of 2, respectively 3, parameters, we will have
6 and 9 parameters, since we have to define the values of $p_{nd}(rel_i)$,
$p_{pd}(rel_i)$, $p_{nq}(rel_i)$, for $i=0, 1, 2$.









