#!/usr/bin/env python

import sys
import os
import pandas as pd
import argparse


def merge_measurements(df_merge, df_org, df_new):
    if df_merge.empty:
        df_merge = pd.merge(df_org, df_new, \
                        on=['dataset', 'year', 'run',  'topic'])
    else:
        df_merge = pd.merge(df_merge, df_new, \
                        on=['dataset', 'year', 'run',  'topic'])
    return df_merge

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--track_eval_path', type=str, required=True)
    parser.add_argument('--msm_eval_path', type=str, required=True)
    parser.add_argument('--out_file_path', type=str, required=True)
    
    args = parser.parse_args()
    track_eval_path = args.track_eval_path
    msm_eval_path = args.msm_eval_path
    out_file_path = args.out_file_path
    
    assert os.path.exists(track_eval_path)
    assert os.path.exists(msm_eval_path)
    
    if not os.path.exists(out_file_path):
        os.makedirs(out_file_path)

    for track_id in ['dd']: #['dd', 's']:
        in_file = os.path.join(track_eval_path, track_id + '.eval')
        df = pd.read_csv(in_file, sep='\t', lineterminator='\n')
        
        # remove rankings that found zero relevant documents
        #df_org = df[df['sDCGs']!=0]
        
        # combine all measurements into one dataframe per track
        df_merge = pd.DataFrame()
        #
        for f in os.listdir(msm_eval_path):
            if f.startswith('MsM') and f.endswith('.eval'):
                file = os.path.join(msm_eval_path, f)

                df_msm = pd.read_csv(file, sep='\t', lineterminator='\n')

                df_merge = merge_measurements(df_merge, df, df_msm)

        # path, name, args
        out_file = os.path.join(out_file_path, track_id + '_all.eval')
        try:
            os.remove(out_file)
        except OSError:
            pass

        df_merge.to_csv(out_file, sep='\t')

if __name__ == "__main__":
    sys.exit(main())
