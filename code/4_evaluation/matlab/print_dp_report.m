%% print_dp_report
% 
% Reports the overall summary of discriminative power analyses for the 
% different tracks and measures and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = print_dp_report()
%  
% *Parameters*
%
% Nothing
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = print_dp_report()

    % load common parameters
    common_parameters

    
    % start of overall computations
    startComputation = tic;
    
    
    fprintf('\n\n######## Reporting overall summary on discriminative power analyses (%s) ########\n\n', ...
        EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - discriminative power:\n');
    fprintf('    * method %s\n', EXPERIMENT.analysis.dp.method);
    fprintf('    * samples %d\n', EXPERIMENT.analysis.dp.samples);
    fprintf('    * alpha %3.2f\n', EXPERIMENT.analysis.dp.alpha);    
    fprintf('  - tracks:\n');
    for t = 1:EXPERIMENT.track.number        
        fprintf('    * track %s\n', EXPERIMENT.track.list{t});
    end    
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', 1, EXPERIMENT.measure.getAcronym(1));
    fprintf('    * end measure: %d (%s)\n\n', EXPERIMENT.measure.number, EXPERIMENT.measure.getAcronym(EXPERIMENT.measure.number));

        
    fprintf('+ Printing the report\n');
    
    % the file where the report has to be written
    reportID = EXPERIMENT.pattern.identifier.discPow.report();
    fid = fopen(EXPERIMENT.pattern.file.report('overall', reportID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    %fprintf(fid, '\\usepackage[a1paper,landscape]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
     fprintf(fid, '\\title{Overall Summary Report on Discriminative Power Analyses}\n\n');
    
    fprintf(fid, '\\author{Nicola Ferro}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    
    fprintf(fid, 'Settings:\n');
    fprintf(fid, '\\begin{itemize}\n');    

    fprintf(fid, '\\item discriminative power:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item method: %s\n', EXPERIMENT.analysis.dp.method);
    fprintf(fid, '\\item samples: %d\n', EXPERIMENT.analysis.dp.samples);
    fprintf(fid, '\\item alpha: %3.2f\n', EXPERIMENT.analysis.dp.alpha);
    fprintf(fid, '\\end{itemize}\n');
           
    fprintf(fid, '\\item analysed tracks:\n');
    fprintf(fid, '\\begin{itemize}\n');
    for t = 1:EXPERIMENT.track.number
        fprintf(fid, '\\item %s -- %s;\n', ...
            strrep(EXPERIMENT.track.list{t}, '_', '\_'), EXPERIMENT.track.getName(t));
    end
    fprintf(fid, '\\end{itemize}\n');
        
    fprintf(fid, '\\item analysed measures:\n');
    fprintf(fid, '\\begin{itemize}\n');
    for m = 1:EXPERIMENT.measure.number        
        fprintf(fid, '\\item %s: %s\n', ...
            strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), strrep(EXPERIMENT.measure.getName(m), '_', '\_'));        
    end
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\end{itemize}\n');
                    
    fprintf(fid, '\\newpage\n');
    
    
     fprintf(fid, '\\begin{table}[p] \n');
    fprintf(fid, '\\centering \n');
    
    fprintf(fid, '\\caption{Discriminative power (DP) and minum required difference ($\\Delta$) for different measures across different tracks.}\n');
    
    fprintf(fid, '\\label{tab:smry-dp}\n');
    
    %fprintf(fid, '\\footnotesize \n');
    
    %fprintf(fid, '\\hspace*{-6.5em} \n');
        
    fprintf(fid, '\\begin{tabular}{|l|*{%d}{r|}} \n', 2*EXPERIMENT.track.number);
    
    fprintf(fid, '\\hline\\hline \n');
    
    fprintf(fid, '\\textbf{Measure} ');
   
    for t = 1:EXPERIMENT.track.number
        fprintf(fid, '& \\multicolumn{2}{c|}{\\textbf{%s}} ', EXPERIMENT.track.list{t});
    end % track
    
    fprintf(fid, '\\\\ \n');
    
    fprintf(fid, '\\cline{%d-%d}\n', 2, 2*EXPERIMENT.track.number + 1);
    
    for t = 1:EXPERIMENT.track.number
        fprintf(fid, '& \\multicolumn{1}{c|}{\\textbf{DP}} & \\multicolumn{1}{c|}{$\\Delta$} ');
    end % track
    
    fprintf(fid, '\\\\ \n');
        
    fprintf(fid, '\\hline \n');
    
    
    % for each measure
    for m = 1:EXPERIMENT.measure.number
        
        mid = EXPERIMENT.measure.list{m};

        fprintf(fid, '%s ', strrep(EXPERIMENT.measure.(mid).acronym, '_', '\_'));
        
        % for each track
        for t = 1:EXPERIMENT.track.number
            
            trackID = EXPERIMENT.track.list{t};
            
            discPowID = EXPERIMENT.pattern.identifier.discPow.analysis(mid, trackID);
            dpID = EXPERIMENT.pattern.identifier.discPow.dp(mid, trackID);
            deltaID = EXPERIMENT.pattern.identifier.discPow.delta(mid, trackID);
            
            try
                serload2(EXPERIMENT.pattern.file.analysis(trackID, discPowID), ...
                    'WorkspaceVarNames', {'dp', 'delta'}, ...
                    'FileVarNames', {dpID, deltaID});
                
                % print the discriminative power and the delat
                fprintf(fid, ' & %5.4f & %5.4f', dp{1, 1}, delta{1, 1});
                                
            catch                
                fprintf(fid, ' & -- & --');            
            end
            
            clear dp delta;
        end    % track
        
        fprintf(fid, '\\\\ \n');
        fprintf(fid, '\\hline \n');

    end % measure
        
            
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\end{tabular} \n');
    
    fprintf(fid, '\\end{table} \n\n');
        
    fprintf(fid, '\\end{document} \n\n');
        
    fclose(fid);
    
               
    fprintf('\n\n######## Total elapsed time for reporting overall summary discriminative power analyses (%s): %s ########\n\n', ...
            EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
