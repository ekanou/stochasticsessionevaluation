%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

diary off;

%% Path Configuration

% if we are running on the cluster 
if (strcmpi(computer, 'GLNXA64'))
    addpath('/nas1/promise/ims/ferro/matters/base/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/analysis/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/io/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/measure/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/plot/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/util/')
end

% The base path
if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster    
    EXPERIMENT.path.base = '/nas1/promise/ims/ferro/yubin/experiment/';
else % if we are running on the local machine
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2018/SIGIR/Evangelos/experiment/';
end

% The path for the matrices, i.e. a directory containing text files 
% representing evaluation measures computed over different runs
EXPERIMENT.path.matrix = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'matrix', filesep);

% The path for the measures
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);

% The path for analyses
EXPERIMENT.path.analysis = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'analysis', filesep);

% The path for figures
EXPERIMENT.path.figure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'figure', filesep);

% The path for reports
EXPERIMENT.path.report = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'report', filesep);

%% General Configuration

% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'SIGIR 2018 DFFK';


%% Configuration for Tracks

EXPERIMENT.track.list = {'DDT', 'ST', 'ALL'};
EXPERIMENT.track.number = length(EXPERIMENT.track.list);

% Dynamic Domain Track
EXPERIMENT.track.DDT.id = 'DDT';
EXPERIMENT.track.DDT.name = 'Dynamic Domain Track';

% Session Track
EXPERIMENT.track.ST.id = 'ST';
EXPERIMENT.track.ST.name = 'Session Track';

% Combined Track
EXPERIMENT.track.ALL.id = 'ALL';
EXPERIMENT.track.ALL.name = 'Combined Track';

% Returns the name of a track given its index in EXPERIMENT.track.list
EXPERIMENT.track.getName = @(idx) ( EXPERIMENT.track.(EXPERIMENT.track.list{idx}).name ); 



%% Patterns for file names

% TXT - Pattern EXPERIMENT.path.matrix/<trackID>/<matrixID>.csv
EXPERIMENT.pattern.file.matrix = @(trackID, matrixID) sprintf('%1$s%2$s%3$s%4$s.csv', EXPERIMENT.path.matrix, trackID, filesep, matrixID);

% ALL - Pattern <path>/<trackID>/<fileID>.<ext>
EXPERIMENT.pattern.file.general.track = @(path, trackID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s.%5$s', path, trackID, filesep, fileID, ext);

% MAT - Pattern EXPERIMENT.path.measure/<trackID>/<measureID>.mat
EXPERIMENT.pattern.file.measure = @(trackID, measureID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.measure, trackID, measureID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/analysis/<trackID>/<analysisID>.mat
EXPERIMENT.pattern.file.analysis = @(trackID, analysisID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.analysis, trackID, analysisID, 'mat');

% PDF - Pattern EXPERIMENT.path.base/figure/<trackID>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, figureID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.figure, trackID, figureID, 'pdf');

% TEX - Pattern EXPERIMENT.path.base/report/<trackID>/<reportID>
EXPERIMENT.pattern.file.report = @(trackID, reportID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.report, trackID, reportID, 'tex');


%% Patterns for identifiers

% Pattern <trackID>_<measureID>
EXPERIMENT.pattern.identifier.matrix =  @(trackID, measureID) sprintf('%1$s_%2$s', lower(trackID), measureID);

% Pattern <measureID>_<trackID>
EXPERIMENT.pattern.identifier.measure =  @(measureID, trackID) sprintf('%1$s_%2$s', measureID, trackID);

% Pattern <type>_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.analysis.general =  @(type, measureID, trackID) sprintf('%1$s_%2$s_%3$s', type, measureID, trackID);

% Pattern discPow_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.discPow.analysis =  @(measureID, trackID) EXPERIMENT.pattern.identifier.analysis.general('discPow', measureID, trackID);

% Pattern discPow_rep_<trackID>
EXPERIMENT.pattern.identifier.discPow.rep =  @(trackID) EXPERIMENT.pattern.identifier.discPow.analysis('rep', trackID);

% Pattern asl_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.discPow.asl =  @(measureID, trackID) EXPERIMENT.pattern.identifier.analysis.general('asl', measureID, trackID);

% Pattern dp_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.discPow.dp =  @(measureID, trackID) EXPERIMENT.pattern.identifier.analysis.general('dp', measureID, trackID);

% Pattern delta_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.discPow.delta =  @(measureID, trackID) EXPERIMENT.pattern.identifier.analysis.general('delta', measureID, trackID);

% Pattern discPow_summary_report
EXPERIMENT.pattern.identifier.discPow.report =  @() EXPERIMENT.pattern.identifier.analysis.general('discPow', 'summary', 'report');

% Pattern corr_tau_<trackID>
EXPERIMENT.pattern.identifier.correlation.analysis =  @(trackID) EXPERIMENT.pattern.identifier.analysis.general('corr', 'tau', trackID);

% Pattern corr_summary_report
EXPERIMENT.pattern.identifier.correlation.report =  @() EXPERIMENT.pattern.identifier.analysis.general('corr', 'summary', 'report');



%% Configuration for measures

% The list of measures under experimentation
EXPERIMENT.measure.list = {'sDCGs', 'sDCG',  'nsDCG', 'nsDCGs',  'EU', 'nEU', 'EUs', 'nEUs', 'CT', 'nCT', 'CTs', 'nCTs', ...
    'MsM_1a', 'MsM_1b', 'MsM_1c', 'MsM_2a', 'MsM_2b', 'MsM_3a', 'MsM_3b', 'MsM_3c', 'MsM_3d', 'MsM_3e', 'MsM_3f', 'MsM_3g', ...
    'MsM_log_1a', 'MsM_log_1b', 'MsM_log_1c', 'MsM_log_2a', 'MsM_log_2b', 'MsM_log_3a', 'MsM_log_3b', 'MsM_log_3c', 'MsM_log_3d', 'MsM_log_3e', 'MsM_log_3f', 'MsM_log_3g'};
EXPERIMENT.measure.number = length(EXPERIMENT.measure.list);

% Configuration for sDCG
EXPERIMENT.measure.sDCG.id = 'sDCG';
EXPERIMENT.measure.sDCG.acronym = 'sDCG';
EXPERIMENT.measure.sDCG.name = 'Session DCG';

% Configuration for nsDCG
EXPERIMENT.measure.nsDCG.id = 'nsDCG';
EXPERIMENT.measure.nsDCG.acronym = 'nsDCG';
EXPERIMENT.measure.nsDCG.name = 'Normalized Session DCG';

% Configuration for sDCGs
EXPERIMENT.measure.sDCGs.id = 'sDCGs';
EXPERIMENT.measure.sDCGs.acronym = 'sDCGs';
EXPERIMENT.measure.sDCGs.name = 'Simplified DCG';

% Configuration for nsDCGs
EXPERIMENT.measure.nsDCGs.id = 'nsDCGs';
EXPERIMENT.measure.nsDCGs.acronym = 'nsDCGs';
EXPERIMENT.measure.nsDCGs.name = 'Normalized Simplified Session DCG';

% Configuration for EU
EXPERIMENT.measure.EU.id = 'EU';
EXPERIMENT.measure.EU.acronym = 'EU';
EXPERIMENT.measure.EU.name = 'Expected Utility';

% Configuration for nEU
EXPERIMENT.measure.nEU.id = 'nEU';
EXPERIMENT.measure.nEU.acronym = 'nEU';
EXPERIMENT.measure.nEU.name = 'Normalized Expected Utility';

% Configuration for EUs
EXPERIMENT.measure.EUs.id = 'EUs';
EXPERIMENT.measure.EUs.acronym = 'EUs';
EXPERIMENT.measure.EUs.name = 'Simplified Expected Utility';

% Configuration for nEUs
EXPERIMENT.measure.nEUs.id = 'nEUs';
EXPERIMENT.measure.nEUs.acronym = 'nEUs';
EXPERIMENT.measure.nEUs.name = 'Normalized Simplified Expected Utility';

% Configuration for CT
EXPERIMENT.measure.CT.id = 'CT';
EXPERIMENT.measure.CT.acronym = 'CT';
EXPERIMENT.measure.CT.name = 'Cube Test';

% Configuration for nCT
EXPERIMENT.measure.nCT.id = 'nCT';
EXPERIMENT.measure.nCT.acronym = 'nCT';
EXPERIMENT.measure.nCT.name = 'Normalized Cube Test';

% Configuration for CTs
EXPERIMENT.measure.CTs.id = 'CTs';
EXPERIMENT.measure.CTs.acronym = 'CTs';
EXPERIMENT.measure.CTs.name = 'Simplified Cube Test';

% Configuration for nCTs
EXPERIMENT.measure.nCTs.id = 'nCTs';
EXPERIMENT.measure.nCTs.acronym = 'nCTs';
EXPERIMENT.measure.nCTs.name = 'Normalized Simplified Cube Test';

% Configuration for MsM_1a
EXPERIMENT.measure.MsM_1a.id = 'MsM_1a';
EXPERIMENT.measure.MsM_1a.acronym = 'MsM_1a';
EXPERIMENT.measure.MsM_1a.name = 'Multi-session Markov 1a';

% Configuration for MsM_1b
EXPERIMENT.measure.MsM_1b.id = 'MsM_1b';
EXPERIMENT.measure.MsM_1b.acronym = 'MsM_1b';
EXPERIMENT.measure.MsM_1b.name = 'Multi-session Markov 1b';

% Configuration for MsM_1c
EXPERIMENT.measure.MsM_1c.id = 'MsM_1c';
EXPERIMENT.measure.MsM_1c.acronym = 'MsM_1c';
EXPERIMENT.measure.MsM_1c.name = 'Multi-session Markov 1c';

% Configuration for MsM_2a
EXPERIMENT.measure.MsM_2a.id = 'MsM_2a';
EXPERIMENT.measure.MsM_2a.acronym = 'MsM_2a';
EXPERIMENT.measure.MsM_2a.name = 'Multi-session Markov 2a';

% Configuration for MsM_2b
EXPERIMENT.measure.MsM_2b.id = 'MsM_2b';
EXPERIMENT.measure.MsM_2b.acronym = 'MsM_2b';
EXPERIMENT.measure.MsM_2b.name = 'Multi-session Markov 2b';

% Configuration for MsM_3a
EXPERIMENT.measure.MsM_3a.id = 'MsM_3a';
EXPERIMENT.measure.MsM_3a.acronym = 'MsM_3a';
EXPERIMENT.measure.MsM_3a.name = 'Multi-session Markov 3a';

% Configuration for MsM_3b
EXPERIMENT.measure.MsM_3b.id = 'MsM_3b';
EXPERIMENT.measure.MsM_3b.acronym = 'MsM_3b';
EXPERIMENT.measure.MsM_3b.name = 'Multi-session Markov 3b';

% Configuration for MsM_3c
EXPERIMENT.measure.MsM_3c.id = 'MsM_3c';
EXPERIMENT.measure.MsM_3c.acronym = 'MsM_3c';
EXPERIMENT.measure.MsM_3c.name = 'Multi-session Markov 3c';

% Configuration for MsM_3d
EXPERIMENT.measure.MsM_3d.id = 'MsM_3d';
EXPERIMENT.measure.MsM_3d.acronym = 'MsM_3d';
EXPERIMENT.measure.MsM_3d.name = 'Multi-session Markov 3d';

% Configuration for MsM_3e
EXPERIMENT.measure.MsM_3e.id = 'MsM_3e';
EXPERIMENT.measure.MsM_3e.acronym = 'MsM_3e';
EXPERIMENT.measure.MsM_3e.name = 'Multi-session Markov 3e';

% Configuration for MsM_3f
EXPERIMENT.measure.MsM_3f.id = 'MsM_3f';
EXPERIMENT.measure.MsM_3f.acronym = 'MsM_3f';
EXPERIMENT.measure.MsM_3f.name = 'Multi-session Markov 3f';

% Configuration for MsM_3g
EXPERIMENT.measure.MsM_3g.id = 'MsM_3g';
EXPERIMENT.measure.MsM_3g.acronym = 'MsM_3g';
EXPERIMENT.measure.MsM_3g.name = 'Multi-session Markov 3g';

% Configuration for MsM_log_1a
EXPERIMENT.measure.MsM_log_1a.id = 'MsM_log_1a';
EXPERIMENT.measure.MsM_log_1a.acronym = 'MsM_log_1a';
EXPERIMENT.measure.MsM_log_1a.name = 'Multi-session Markov 1a';

% Configuration for MsM_log_1b
EXPERIMENT.measure.MsM_log_1b.id = 'MsM_log_1b';
EXPERIMENT.measure.MsM_log_1b.acronym = 'MsM_log_1b';
EXPERIMENT.measure.MsM_log_1b.name = 'Multi-session Markov 1b';

% Configuration for MsM_log_1c
EXPERIMENT.measure.MsM_log_1c.id = 'MsM_log_1c';
EXPERIMENT.measure.MsM_log_1c.acronym = 'MsM_log_1c';
EXPERIMENT.measure.MsM_log_1c.name = 'Multi-session Markov 1c';

% Configuration for MsM_log_2a
EXPERIMENT.measure.MsM_log_2a.id = 'MsM_log_2a';
EXPERIMENT.measure.MsM_log_2a.acronym = 'MsM_log_2a';
EXPERIMENT.measure.MsM_log_2a.name = 'Multi-session Markov 2a';

% Configuration for MsM_log_2b
EXPERIMENT.measure.MsM_log_2b.id = 'MsM_log_2b';
EXPERIMENT.measure.MsM_log_2b.acronym = 'MsM_log_2b';
EXPERIMENT.measure.MsM_log_2b.name = 'Multi-session Markov 2b';

% Configuration for MsM_log_3a
EXPERIMENT.measure.MsM_log_3a.id = 'MsM_log_3a';
EXPERIMENT.measure.MsM_log_3a.acronym = 'MsM_log_3a';
EXPERIMENT.measure.MsM_log_3a.name = 'Multi-session Markov 3a';

% Configuration for MsM_log_3b
EXPERIMENT.measure.MsM_log_3b.id = 'MsM_log_3b';
EXPERIMENT.measure.MsM_log_3b.acronym = 'MsM_log_3b';
EXPERIMENT.measure.MsM_log_3b.name = 'Multi-session Markov 3b';

% Configuration for MsM_log_3c
EXPERIMENT.measure.MsM_log_3c.id = 'MsM_log_3c';
EXPERIMENT.measure.MsM_log_3c.acronym = 'MsM_log_3c';
EXPERIMENT.measure.MsM_log_3c.name = 'Multi-session Markov 3c';

% Configuration for MsM_log_3d
EXPERIMENT.measure.MsM_log_3d.id = 'MsM_log_3d';
EXPERIMENT.measure.MsM_log_3d.acronym = 'MsM_log_3d';
EXPERIMENT.measure.MsM_log_3d.name = 'Multi-session Markov 3d';

% Configuration for MsM_log_3e
EXPERIMENT.measure.MsM_log_3e.id = 'MsM_log_3e';
EXPERIMENT.measure.MsM_log_3e.acronym = 'MsM_log_3e';
EXPERIMENT.measure.MsM_log_3e.name = 'Multi-session Markov 3e';

% Configuration for MsM_log_3f
EXPERIMENT.measure.MsM_log_3f.id = 'MsM_log_3f';
EXPERIMENT.measure.MsM_log_3f.acronym = 'MsM_log_3f';
EXPERIMENT.measure.MsM_log_3f.name = 'Multi-session Markov 3f';

% Configuration for MsM_log_3g
EXPERIMENT.measure.MsM_log_3g.id = 'MsM_log_3g';
EXPERIMENT.measure.MsM_log_3g.acronym = 'MsM_log_3g';
EXPERIMENT.measure.MsM_log_3g.name = 'Multi-session Markov 3g';



% Returns the identifier of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getID = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).id ); 

% Returns the acronym of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getAcronym = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).acronym ); 

% Returns the name of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getName = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).name ); 


%% Configuration for analyses

% The significance level for the analyses
EXPERIMENT.analysis.alpha.threshold = 0.05;
EXPERIMENT.analysis.alpha.color = 'lightgrey';


% The correlation analysis
% The list of correlation measures under experimentation
EXPERIMENT.correlation.list = {'tau'};
EXPERIMENT.correlation.number = length(EXPERIMENT.correlation.list);

% Labels of the correlation pairs
EXPERIMENT.correlation.labels.list = EXPERIMENT.measure.list(combnk(1:EXPERIMENT.measure.number, 2));
EXPERIMENT.correlation.labels.first = EXPERIMENT.correlation.labels.list(:, 1);
EXPERIMENT.correlation.labels.second = EXPERIMENT.correlation.labels.list(:, 2);
EXPERIMENT.correlation.labels.list(:, 3) = EXPERIMENT.correlation.labels.list(:, 2);
EXPERIMENT.correlation.labels.list(:, 2) = {'_'};
EXPERIMENT.correlation.labels.list = strcat(EXPERIMENT.correlation.labels.list(:, 1), EXPERIMENT.correlation.labels.list(:, 2), EXPERIMENT.correlation.labels.list(:, 3));
EXPERIMENT.correlation.labels.number = length(EXPERIMENT.correlation.labels.list);

% Configuration for Kendall's tau correlation
EXPERIMENT.correlation.tau.id = 'tau';
EXPERIMENT.correlation.tau.symbol.tex = '\tau';
EXPERIMENT.correlation.tau.symbol.latex = '$\tau$';
EXPERIMENT.correlation.tau.label.tex = 'Kendall''s \tau Correlation';
EXPERIMENT.correlation.tau.label.latex = 'Kendall''s $\tau$ Correlation';
EXPERIMENT.correlation.tau.name = 'Kendall''s tau correlation';
EXPERIMENT.correlation.tau.compute = @(data) corr(data, 'type', 'Kendall');


% The discriminative power analysis
EXPERIMENT.analysis.dp.method = 'PairedBootstrapTest';
EXPERIMENT.analysis.dp.samples = 1000;
EXPERIMENT.analysis.dp.alpha = EXPERIMENT.analysis.alpha.threshold;
EXPERIMENT.analysis.dp.ignoreNaN = true;
EXPERIMENT.analysis.dp.compute.rep = @(measure, replicates) (discriminativePower(measure, 'Replicates', replicates, ...
    'Method', EXPERIMENT.analysis.dp.method, 'Samples', EXPERIMENT.analysis.dp.samples, ...
    'IgnoreNaN', EXPERIMENT.analysis.dp.ignoreNaN, 'Alpha', EXPERIMENT.analysis.dp.alpha));
EXPERIMENT.analysis.dp.compute.norep = @(measure) (discriminativePower(measure, ...
    'Method', EXPERIMENT.analysis.dp.method, 'Samples', EXPERIMENT.analysis.dp.samples, ...
    'IgnoreNaN', EXPERIMENT.analysis.dp.ignoreNaN, 'Alpha', EXPERIMENT.analysis.dp.alpha));




    
