%% import_matrix
%
% Imports the requested measure matrix and saves it to a |.mat| file.
%
%% Synopsis
%
%   [] = import_matrix(trackID, removeNaN, startMeasure, endMeasure)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to import.
% * *|removeNaN|* - whether to keep or remove |NaN| values. If |true|, it 
% substitutes |NaN| values with zeros; if |false| it leaves |NaN| values.
% * *|startMeasure|* - the index of the start measure to process. Optional.
% * *|endMeasure|* - the index of the end measure to process. Optional.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2017a or higher
% * *Copyright:* (C) 2018 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = import_matrix(trackID, removeNaN, startMeasure, endMeasure)

    % check the number of input arguments
    narginchk(2, 4);

    % set up the common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that removeNaN is a logical scalar
    validateattributes(removeNaN, {'logical'}, ...
        {'nonempty', 'scalar'}, '', 'removeNaN');
            
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    if nargin == 4
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Importing matrices %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - imported on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - remove NaN values: %s\n', char(removeNaN*'true ' + (1-removeNaN)*'false'));
    fprintf('  - measures \n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));

    
    % for each measure
    for m = startMeasure:endMeasure
        
        start = tic;
        
        fprintf('+ Importing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
        
        matrixID = EXPERIMENT.pattern.identifier.matrix(trackID, mid);
        measureID = EXPERIMENT.pattern.identifier.measure(mid, trackID);
        
        try
            measure = readtable(EXPERIMENT.pattern.file.matrix(trackID, matrixID), ...
                'ReadVariableNames', true, 'ReadRowNames', true);
            
            % replace NaN with 0, if requested
            if (removeNaN)
                measure = fillmissing(measure, 'constant', 0);
            end
            
            measure.Properties.UserData.identifier = measureID;
            measure.Properties.UserData.name = EXPERIMENT.measure.getName(m);
            measure.Properties.UserData.shortName = measureID;
            measure.Properties.UserData.pool = trackID;
            
            sersave2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'measure'}, ...
                'FileVarNames', {measureID});
            
            clear measure;
        catch
            fprintf('  - %s not available for track %s\n', EXPERIMENT.measure.getAcronym(m), trackID);
        end
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end % for measure

    fprintf('\n\n######## Total elapsed time for importing collection %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

 
end


