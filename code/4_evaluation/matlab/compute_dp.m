%% compute_dp
%
% Computes the discriminative power and saves it to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_dp(trackID, useReplicates, startMeasure, endMeasure)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to import.
% * *|useReplicates|* - whether to use existing replicates (|true|) or not
% (|false|). If |true| and replicates do not exist yet or |false|, it will
% create new replicates. It always save the currently used replicates.
% * *|startMeasure|* - the index of the start measure to process. Optional.
% * *|endMeasure|* - the index of the end measure to process. Optional.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2017a or higher
% * *Copyright:* (C) 2018 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_dp(trackID, useReplicates, startMeasure, endMeasure)

    % check the number of input arguments
    narginchk(2, 3);

    % set up the common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    % check that useReplicates is a logical scalar
    validateattributes(useReplicates, {'logical'}, ...
        {'nonempty', 'scalar'}, '', 'useReplicates');
    
    if nargin == 4
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing discriminative power on %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - use existing replicates, if any: %s\n', char(useReplicates*'true ' + (1-useReplicates)*'false'));
    fprintf('  - measures \n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    fprintf('\n');
    
    % for each measure
    for m = startMeasure:endMeasure
        
        start = tic;
        
        fprintf('+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
        
        measureID = EXPERIMENT.pattern.identifier.measure(mid, trackID);
                
        discPowID = EXPERIMENT.pattern.identifier.discPow.analysis(mid, trackID);
        repID = EXPERIMENT.pattern.identifier.discPow.rep(trackID);
        aslID = EXPERIMENT.pattern.identifier.discPow.asl(mid, trackID);
        dpID = EXPERIMENT.pattern.identifier.discPow.dp(mid, trackID);
        deltaID = EXPERIMENT.pattern.identifier.discPow.delta(mid, trackID);
        
        try
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'measure'}, ...
                'FileVarNames', {measureID});
            
            if (useReplicates)
                try
                    serload2(EXPERIMENT.pattern.file.analysis(trackID, repID), ...
                        'WorkspaceVarNames', {'replicates'}, ...
                        'FileVarNames', {repID});
                catch
                    fprintf('  - replicates not available yet for track %s\n', trackID);
                    replicates = [];
                end
                
            else
                replicates = [];
            end
            
            if (isempty(replicates))
                [asl, dp, delta, replicates] = EXPERIMENT.analysis.dp.compute.norep(measure);
            else
                [asl, dp, delta, replicates] = EXPERIMENT.analysis.dp.compute.rep(measure, replicates);
            end           
            
            sersave2(EXPERIMENT.pattern.file.analysis(trackID, discPowID), ...
                'WorkspaceVarNames', {'asl', 'dp', 'delta'}, ...
                'FileVarNames', {aslID, dpID, deltaID});
            
            sersave2(EXPERIMENT.pattern.file.analysis(trackID, repID), ...
                'WorkspaceVarNames', {'replicates'}, ...
                'FileVarNames', {repID});
            
            clear measure asl dp delta replicates;
        catch
            fprintf('  - %s not available for track %s\n', EXPERIMENT.measure.getAcronym(m), trackID);
        end
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end % for measure

    fprintf('\n\n######## Total elapsed time for computing discriminative power on %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

 
end


