%% import_matrix
%
% Computes the discriminative power and saves it to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_dp(trackID, useReplicates, startMeasure, endMeasure)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to import.
% * *|useReplicates|* - whether to use existing replicates (|true|) or not
% (|false|). If |true| and replicates do not exist yet or |false|, it will
% create new replicates. It always save the currently used replicates.
% * *|startMeasure|* - the index of the start measure to process. Optional.
% * *|endMeasure|* - the index of the end measure to process. Optional.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2017a or higher
% * *Copyright:* (C) 2018 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_correlation(trackID)

    % check the number of input arguments
    narginchk(1, 1);

    % set up the common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
 
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing correlation analysis on %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', 1, EXPERIMENT.measure.getAcronym(1));
    fprintf('    * end measure: %d (%s)\n\n', EXPERIMENT.measure.number, EXPERIMENT.measure.getAcronym(EXPERIMENT.measure.number));
    fprintf('\n');
    
    fprintf('+ Loading measures and ranking systems\n');
        
    data = [];
    
    % for each measure
    for m = 1:EXPERIMENT.measure.number         
        mid = EXPERIMENT.measure.list{m};
        
        measureID = EXPERIMENT.pattern.identifier.measure(mid, trackID);
                        
        try
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'measure'}, ...
                'FileVarNames', {measureID});
                     
            data = [data nanmean(measure{:, :}).'];
                                 
            clear measure;
        catch
            fprintf('  - %s not available for track %s\n', EXPERIMENT.measure.getAcronym(m), trackID);
            data = [data NaN(size(data, 1), 1)];
        end                
    end % for measure
    
    fprintf('+ Computing Kendall''s tau correlation\n');
    
    % compute the Kendall's tau correlation
    correlation = EXPERIMENT.correlation.tau.compute(data);
            
    % indexes of the upper triangle
    idx = logical(tril(ones(EXPERIMENT.measure.number), -1));
    
    %extract the upper triangle
    correlation = correlation(idx).';
        
    corrID = EXPERIMENT.pattern.identifier.correlation.analysis(trackID);
    
    sersave2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
                    'WorkspaceVarNames', {'correlation'}, ...
                    'FileVarNames', {corrID});
    
    
    
    fprintf('\n\n######## Total elapsed time for computing correlation analysis on %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

 
end


