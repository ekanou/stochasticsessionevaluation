#!/usr/bin/env python
import sys
import os

import matplotlib
matplotlib.use('tkAgg')
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns
import numpy as np
import argparse
import functools

def get_results(piv, metric, result):
    piv_m = piv.fillna(0).mean().to_frame().reset_index()
    piv_m.columns = ['run', metric]
    if result.empty:
        result = piv_m
    else:
        result = pd.merge(result, piv_m, on=['run'])
    return result

def save_plot(corr, prefix):
    # Generate a mask for the upper triangle
    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    plt.subplots(figsize=(20,15))
    sns.set(font_scale=1.5)
    sns_plot = sns.heatmap(corr,
            mask=mask,
            square=True, linewidths=.5,
            annot=True, fmt='.2g', annot_kws={"size": 20})
    sns_plot.tick_params(labelsize=25)
    plt.yticks(rotation=0)
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.savefig('correlation/' + prefix + '_kendall.png')
    plt.close()


def convert(m):
    forward = {'fwd':100,'bck':200}
    patience = {'imp':10,'bal':20,'pat':30}
    reform = {'down':1,'bal':2,'reform':3}
    tmp = m.split('_')
    return int(forward[tmp[1]] + patience[tmp[2]] + reform[tmp[3]])
              
    
def compare(m1,m2):
    m1c = convert(m1)
    m2c = convert(m2)
    return m1c - m2c

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--track_eval_path', type=str, required=True)
    parser.add_argument('--track', type=str, required=True)
    parser.add_argument('--year', type=str, required=True)
    parser.add_argument('--disc', type=str, required=True)
    parser.add_argument('--mod', type=str, required=True)
    parser.add_argument('--pat', type=str, required=True)
    parser.add_argument('--soa', type=str, required=True)
    
    
    args = parser.parse_args()
    track_eval_path = args.track_eval_path
    track = args.track
    year = args.year
    disc = args.disc
    mod = args.mod
    pat = args.pat
    soa = args.soa
    
    assert os.path.exists(track_eval_path)
    
    csv_file = os.path.join(track_eval_path, track + '_all.eval')
    df = pd.read_csv(csv_file, sep='\t', lineterminator='\n')

    result = pd.DataFrame()

    #metrics = ['sDCG' , 'nsDCG' ,'sDCGs', 'nsDCGs', 'EU' ,  'nEU' , 'EUs', 'nEUs', 'CT', 'nCT',  'CTs', 'nCTs']
    metrics = ['nsDCGs' , 'nEUs' , 'nCTs']
    MsM = ['MsM_bck_imp_bal_lin','MsM_bck_imp_bal_log','MsM_bck_bal_bal_lin','MsM_bck_bal_bal_log','MsM_fwd_bal_reform_lin','MsM_fwd_bal_reform_log','MsM_fwd_bal_bal_lin','MsM_fwd_bal_bal_log','MsM_fwd_pat_down_lin','MsM_fwd_pat_down_log','MsM_bck_bal_reform_lin','MsM_bck_bal_reform_log','MsM_fwd_imp_bal_lin','MsM_fwd_imp_bal_log','MsM_bck_bal_down_lin','MsM_bck_bal_down_log','MsM_fwd_imp_down_lin','MsM_fwd_imp_down_log','MsM_bck_imp_down_lin','MsM_bck_imp_down_log','MsM_bck_imp_reform_lin','MsM_bck_imp_reform_log','MsM_fwd_bal_down_lin','MsM_fwd_bal_down_log','MsM_fwd_imp_reform_lin','MsM_fwd_imp_reform_log','MsM_fwd_pat_bal_lin','MsM_fwd_pat_bal_log','MsM_fwd_pat_reform_lin','MsM_fwd_pat_reform_log','MsM_bck_pat_down_lin','MsM_bck_pat_down_log','MsM_bck_pat_reform_lin','MsM_bck_pat_reform_log','MsM_bck_pat_bal_lin','MsM_bck_pat_bal_log']

    MsMsorted = sorted(MsM, key=functools.cmp_to_key(compare))
    metrics = metrics + MsMsorted
    years = [2015, 2016, 2017]
    
    if year != 'all':
        df = df[df['year']==int(year)]

        for metric in metrics:

            tmp = metric.split('_')
            if soa == 'no' and len(tmp)==1: continue
            if soa == 'only' and len(tmp)>1: continue
            if mod != 'all' and len(tmp)>1 and tmp[1] != mod: continue
            if pat != 'all' and len(tmp)>1 and tmp[2] != pat: continue
            if disc != 'all' and len(tmp)>1 and tmp[4] != disc: continue
            
            piv = df.pivot(index='topic', columns='run', values=metric)
            result = get_results(piv, metric, result)

            corr = result.corr(method='kendall')
            
        save_plot(corr,  track + '_' + year + '_' + soa + '_' + disc + '_' + mod + '_' + pat + '_')

    else:

        all_corr = pd.DataFrame()
        for y in years:
            result = pd.DataFrame()
            df_y = df[df['year']==int(y)]
            for metric in metrics:
                
                tmp = metric.split('_')
                if soa == 'no' and len(tmp)==1: continue
                if soa == 'only' and len(tmp)>1: continue
                if mod != 'all' and len(tmp)>1 and tmp[1] != mod: continue
                if pat != 'all' and len(tmp)>1 and tmp[2] != pat: continue
                if disc != 'all' and len(tmp)>1 and tmp[4] != disc: continue


                piv = df_y.pivot(index='topic', columns='run', values=metric)
                result = get_results(piv, metric, result)
                corr = result.corr(method='kendall')

            if all_corr.empty:
                all_corr = corr
            else:
                all_corr = all_corr.add(corr)

        corr = np.divide(all_corr,3)
        save_plot(corr,  track + '_' + year + '_' + soa + '_' + disc + '_' + mod + '_' + pat + '_')
                



if __name__ == "__main__":
    sys.exit(main())
