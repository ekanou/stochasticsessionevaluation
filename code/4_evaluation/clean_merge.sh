#!/usr/bin/env bash


python clean_merge.py \
    --track_eval_path "../9_data/measurements/" \
    --msm_eval_path "../9_data/measurements/" \
    --out_file_path "../9_data/measurements/" \
