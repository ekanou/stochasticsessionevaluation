#!/usr/bin/env python
import sys

import pandas as pd

def main():

    #df_st = pd.read_csv('../9_data/measurements/s_all.eval', sep='\t', lineterminator='\n', index_col=1)
    df_dd = pd.read_csv('../9_data/measurements/DD_all.eval', sep='\t', lineterminator='\n', index_col=1)
    
#    per metric

    cols = list(df_dd)
    non_metrics = ['Unnamed: 0', 'dataset', 'year', 'run', 'topic']
    metrics = [m for m in cols if m not in non_metrics]
    years = df_dd.year.unique()
    
    for metric in metrics:
        for year in years:
            #        p_st = df_st.pivot(index='topic', columns='run', values=metric)
            #        p_st.to_csv('./matrices/s_' + metric + '.csv' , sep='\t')

            
            p_dd_y = df_dd[df_dd['year']==year]

            p_dd = p_dd_y.pivot(index='topic', columns='run', values=metric)
            p_dd.to_csv('./matrices/' + 'dd' + str(year) + '_' + metric + '.csv' , sep='\t')

#        p_concat = pd.concat([p_st, p_dd], axis=0)
#        p_concat.to_csv('./matrices/all_' + metric + '.csv' , sep='\t')

#    for metric in ['sDCG', 'nsDCG', 'EU', 'nEU', 'CT', 'nCT']:
#        p_dd = df_dd.pivot(index='topic', columns='run', values=metric)
#        p_dd.to_csv('./matrices/ddt_' + metric + '.csv' , sep='\t')

if __name__ == "__main__":
    sys.exit(main())
