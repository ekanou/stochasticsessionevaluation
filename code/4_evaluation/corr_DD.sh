#!/usr/bin/env bash

if [0]
then

# Correlations between simple, normalized, and unnormalized versions of SOTA, as well as across SOTA
python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "all" \
    --disc "lin" \
    --mod "fwd" \
    --pat "all" \
    --soa "only"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2015" \
    --disc "lin" \
    --mod "fwd" \
    --pat "all" \
    --soa "only"
python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2016" \
    --disc "lin" \
    --mod "fwd" \
    --pat "all" \
    --soa "only"
python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2017" \
    --disc "lin" \
    --mod "fwd" \
    --pat "all" \
    --soa "only"

fi


# Correlations between linear, forward model MsM and SOTA
python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2015" \
    --disc "lin" \
    --mod "fwd" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2016" \
    --disc "lin" \
    --mod "fwd" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2017" \
    --disc "lin" \
    --mod "fwd" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "all" \
    --disc "lin" \
    --mod "fwd" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2015" \
    --disc "log" \
    --mod "fwd" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2016" \
    --disc "log" \
    --mod "fwd" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2017" \
    --disc "log" \
    --mod "fwd" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "all" \
    --disc "log" \
    --mod "fwd" \
    --pat "all" \
    --soa "yes"

if [0]
then
python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2015" \
    --disc "lin" \
    --mod "fwd" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2016" \
    --disc "lin" \
    --mod "bck" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2017" \
    --disc "lin" \
    --mod "bck" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "all" \
    --disc "lin" \
    --mod "bck" \
    --pat "all" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2015" \
    --disc "lin" \
    --mod "all" \
    --pat "bal" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2016" \
    --disc "lin" \
    --mod "all" \
    --pat "bal" \
    --soa "yes"

python corr.py \
    --track_eval_path "../9_data/measurements/" \
    --track "dd" \
    --year "2017" \
    --disc "lin" \
    --mod "all" \
    --pat "bal" \
    --soa "yes"
fi
