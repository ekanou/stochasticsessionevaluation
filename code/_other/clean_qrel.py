import sys
import pyndri
from cvangysel import argparse_utils
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--qrel_in',
                        type=argparse_utils.existing_file_path,
                        required=True)
                        
    parser.add_argument('--index', type=argparse_utils.existing_file_path,
                        required=True)

    parser.add_argument('--qrel_out',
                        type=argparse_utils.nonexisting_file_path,
                        required=True)

    args = parser.parse_args()

    index = pyndri.Index(args.index)
    
    with open(args.qrel_in, 'r') as f_in:
        with open(args.qrel_out, 'w') as f_out:
            for line in f_in:
                line = line.strip()
        
                if not line:
                    continue

                try:
                    data = line.strip().split()[:4]
                except:
                    continue

                session_id, _, doc_no, relevance  = data
                
                if index.get_document_text(doc_no) != '':
                    f_out.write(
                        '{session_id} 0 {document_id} {relevance}\n'.format(
                            session_id=session_id,
                            document_id=doc_no,
                            relevance=relevance))

            


if __name__ == "__main__":
    sys.exit(main())
