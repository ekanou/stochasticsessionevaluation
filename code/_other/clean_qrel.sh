#!/usr/bin/env bash

set -e

python clean_qrel.py --qrel_in /Users/davidvandijk/Work/Sigir2018/sesh/scratch/2011/2011.qrel --index /Volumes/WD_D/files/09B/ClueWeb09_English_1/ --qrel_out /Users/davidvandijk/Work/Sigir2018/sesh/scratch/2011/2011c.qrel

python clean_qrel.py --qrel_in /Users/davidvandijk/Work/Sigir2018/sesh/scratch/2013/2013.qrel --index /Volumes/WD_D/files/12b/ClueWeb12b --qrel_out /Users/davidvandijk/Work/Sigir2018/sesh/scratch/2013/2013c.qrel

python clean_qrel.py --qrel_in /Users/davidvandijk/Work/Sigir2018/sesh/scratch/2014/2014.qrel --index /Volumes/WD_D/files/12b/ClueWeb12b --qrel_out /Users/davidvandijk/Work/Sigir2018/sesh/scratch/2014/2014c.qrel
