#!/usr/bin/env python

import sys
import os
import pandas as pd
import xml.etree.ElementTree as ET

def main():
    root = ET.parse('/Users/davidvandijk/Work/Sigir2018/sesh/scratch/2013/2013.xml').getroot()
    session_numq = dict()
    for session in list(root):
        num_q_sess = 0
        session_num = session.attrib['num']
        if int(session_num) > 87:
            continue
        for tag_under_session in list(session):
            if tag_under_session.tag == 'currentquery':
                num_q_sess += 1
            if tag_under_session.tag == 'interaction':
                interaction = tag_under_session
                interaction_num = interaction.attrib['num']
                for tag_under_interaction in list(interaction):
                    if tag_under_interaction.tag == 'query':
                        query = tag_under_interaction
                        query_text = query.text
                        num_q_sess += 1
        
        session_numq[session_num] = num_q_sess

    df = pd.DataFrame.from_dict(session_numq, orient='index')

    print('#sessions', df.count())
    print('#queries', df.sum())
    print('mean q per s', df.mean())
    print('std', df.std())
    print('median', df.median())


if __name__ == "__main__":
    sys.exit(main())
