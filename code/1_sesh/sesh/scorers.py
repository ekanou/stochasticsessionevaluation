from __future__ import unicode_literals

from cvangysel import io_utils, rank_utils
from sesh import domain, sesh_pb2

import collections
import json
import logging
import nltk
import numpy as np
import operator
import string
import pyndri

def create_scorer(scorer_desc, qrels_per_session=None):
    return SESSION_SCORERS[scorer_desc.type](
        scorer_desc, qrels_per_session)


class SessionScorer(object):

    def __init__(self, desc, qrels_per_session, encoding='utf8'):
        self.desc = desc
        self.DO_NOT_USE_qrels = qrels_per_session

        self.encoding = encoding

    def __repr__(self):
        return '<SessionScorer {0}>'.format(str(self.desc).replace('\n', ' '))

    def score_documents_for_session(
            self,
            session,
            candidate_document_ids,
            index, dictionary,
            anchor_texts,
            word_frequency_index,
            logger,
            f_debug_out):
        raise NotImplementedError()


class QrelOracleScorer(SessionScorer):

    def score_documents_for_session(
            self,
            session,
            candidate_document_ids,
            index, dictionary,
            anchor_texts,
            word_frequency_index,
            logger,
            f_debug_out):
        if session.session_id not in self.DO_NOT_USE_qrels[0].keys():
            return None

        for document_id, relevance in \
                self.DO_NOT_USE_qrels[0][session.session_id]:
            yield document_id, relevance



class IndriScorer(SessionScorer):

    def __init__(self, *args, **kwargs):
        super(IndriScorer, self).__init__(*args, **kwargs)

        self.exclude = set(string.punctuation)

        assert self.desc.indri_scorer_desc
        assert self.desc.indri_scorer_desc.query_position in (
            sesh_pb2.IndriScorerDesc.FIRST,
            sesh_pb2.IndriScorerDesc.LAST,
            sesh_pb2.IndriScorerDesc.ALL,
            sesh_pb2.IndriScorerDesc.THEME,
            sesh_pb2.IndriScorerDesc.CUSTOM)
            
        assert self.desc.indri_scorer_desc.query_environment in (
            sesh_pb2.IndriScorerDesc.INDRI,
            sesh_pb2.IndriScorerDesc.LM,
            sesh_pb2.IndriScorerDesc.TFIDF,
            sesh_pb2.IndriScorerDesc.OKAPI)

        if self.desc.indri_scorer_desc.query_position == \
                sesh_pb2.IndriScorerDesc.CUSTOM:
            assert self.desc.indri_scorer_desc.weights_path

            with open(self.desc.indri_scorer_desc.weights_path, 'r') \
                    as f_weights:
                self.term_weights = json.loads(f_weights.read())
        else:
            self.term_weights = None

    def score_documents_for_session(
            self,
            session,
            candidate_document_ids,
            index, dictionary,
            anchor_texts,
            word_frequency_index,
            logger,
            f_debug_out):
        if self.desc.indri_scorer_desc.query_position in (
                sesh_pb2.IndriScorerDesc.FIRST,
                sesh_pb2.IndriScorerDesc.LAST):
            query_idx = (
                -1
                if self.desc.indri_scorer_desc.query_position ==
                sesh_pb2.IndriScorerDesc.LAST
                else 0)

            if self.desc.indri_scorer_desc.cleaned:
                terms = session.interactions[query_idx].query
            else:
                terms = session.interactions[query_idx].original_query.split()

            if self.desc.indri_scorer_desc.binary:
                terms = list(set(terms))

            query = ' '.join(terms)
        elif self.desc.indri_scorer_desc.query_position == \
                sesh_pb2.IndriScorerDesc.ALL:
            if self.desc.indri_scorer_desc.cleaned:
                terms = [
                    term
                    for interaction in session.interactions
                    for term in interaction.query]
            else:
                terms = [
                    term
                    for interaction in session.interactions
                    for term in interaction.original_query.split()]

            if self.desc.indri_scorer_desc.binary:
                terms = list(set(terms))

            query = ' '.join(terms)
        elif self.desc.indri_scorer_desc.query_position == \
                sesh_pb2.IndriScorerDesc.THEME:
            theme_terms = set(session.queries[0])

            for query in session.queries[1:]:
                theme_terms &= set(query)

            if theme_terms:
                query = ' '.join(theme_terms)
            else:
                query = ' '.join(
                    interaction.original_query
                    for interaction in session.interactions)
        elif self.desc.indri_scorer_desc.query_position == \
                sesh_pb2.IndriScorerDesc.CUSTOM:
            assert self.term_weights is not None

            if (str(session.track_edition) not in self.term_weights or
                    str(session.session_id) not in
                    self.term_weights[str(session.track_edition)]):
                return

            query_pieces = []

            for term, weight in self.term_weights[
                    str(session.track_edition)][
                    str(session.session_id)].items():
                query_pieces.append('{:.20f} {}'.format(weight, term))

            query = '#weight( {} )'.format(' '.join(query_pieces))
        else:
            raise NotImplementedError()

        # Strip punctuation.
        if self.desc.indri_scorer_desc.query_position != \
                sesh_pb2.IndriScorerDesc.CUSTOM:
            query = ''.join(ch for ch in query if ch not in self.exclude)

        f_debug_out.write('Query: {}\n'.format(query))

        logger.debug('Indri query: %s', query)
        
        query_env = None
        if self.desc.indri_scorer_desc.query_environment == \
                sesh_pb2.IndriScorerDesc.INDRI:
            results = index.query(
                    query,
                    document_set=map(operator.itemgetter(1),
                             index.document_ids(candidate_document_ids)))
        elif self.desc.indri_scorer_desc.query_environment == \
                    sesh_pb2.IndriScorerDesc.LM:
                query_env = pyndri.QueryEnvironment(
                        index, rules=('method:dirichlet,mu:5000',))
        elif self.desc.indri_scorer_desc.query_environment == \
                    sesh_pb2.IndriScorerDesc.TFIDF:
                query_env = pyndri.TFIDFQueryEnvironment(index)
        elif self.desc.indri_scorer_desc.query_environment == \
                    sesh_pb2.IndriScorerDesc.OKAPI:
                query_env = pyndri.OkapiQueryEnvironment(index)
        
        if query_env:
            results = query_env.query(
                query,
                document_set=map(operator.itemgetter(1),
                                index.document_ids(candidate_document_ids)))

        logger.debug('Indri returned %d results.', len(results))

        for int_document_id, log_score in results:
            ext_document_id, _ = index.document(int_document_id)

            yield ext_document_id, log_score


class QCMScorer(SessionScorer):

    def __init__(self, *args, **kwargs):
        super(QCMScorer, self).__init__(*args, **kwargs)

        assert self.desc.qcm_scorer_desc

        assert self.desc.qcm_scorer_desc.belief_type in (
            sesh_pb2.AND, sesh_pb2.OR)
            
        assert self.desc.qcm_scorer_desc.query_environment in (
            sesh_pb2.IndriScorerDesc.INDRI,
            sesh_pb2.IndriScorerDesc.LM,
            sesh_pb2.IndriScorerDesc.TFIDF,
            sesh_pb2.IndriScorerDesc.OKAPI)

        # HACK; for now.
        if not self.desc.qcm_scorer_desc.params.alpha:
            self.desc.qcm_scorer_desc.params.alpha = 2.2
            self.desc.qcm_scorer_desc.params.beta = 1.8
            self.desc.qcm_scorer_desc.params.epsilon = 0.07
            self.desc.qcm_scorer_desc.params.delta = 0.4

            self.desc.qcm_scorer_desc.params.gamma = 1.0

        logging.info('QCMScorer parameters: %s',
                     self.desc.qcm_scorer_desc.params)

    def score_documents_for_session(
            self,
            session,
            candidate_document_ids,
            index, dictionary,
            anchor_texts,
            word_frequency_index,
            logger,
            f_debug_out):
        if not session.queries:
            return

        interaction_queries = []

        for transition_idx, (previous_interaction, next_interaction) in \
                enumerate(session.iter_transitions(prepend_start=True)):
            previous_serp = previous_interaction.serp
            previous_serp_lm = \
                previous_interaction.create_snippet_lm(dictionary)

            previous_query = previous_interaction.query
            previous_query_bow = dictionary.doc2bow(previous_query)
    
            next_query = next_interaction.query
            next_query_bow = dictionary.doc2bow(next_query)

            previous_serp_according_to_relevance = sorted(
                ((document.doc_id,
                  previous_serp_lm.score_entity_for_query(
                      document.doc_id, previous_query_bow))
                 for document in previous_serp),
                key=operator.itemgetter(1),
                reverse=True)

            if not previous_serp_according_to_relevance:
                logger.warning('Unable to rank SERP for query "%s".',
                               str(session))

            logger.debug('Re-ranked SERP for "%s": %s',
                         previous_query,
                         previous_serp_according_to_relevance)

            max_reward_document_id, _ = (
                previous_serp_according_to_relevance[0]
                if previous_serp_according_to_relevance else (None, -np.inf))

            logger.debug('Maximum reward document: %s',
                         max_reward_document_id)

            if not next_query_bow:
                logger.warning('Query %s seems to be OoV.', next_query)

            added_term_ids, removed_term_ids, preserved_term_ids = \
                domain.compute_query_change(previous_interaction,
                                            next_interaction,
                                            dictionary)

            theme_term_ids = \
                domain.compute_theme_terms(previous_interaction,
                                           next_interaction,
                                           dictionary)

            all_term_ids = added_term_ids | \
                removed_term_ids | \
                preserved_term_ids | \
                set(theme_term_ids)

            logger.debug('Transition with theme terms %s, '
                         'added terms %s and removed terms %s.',
                         theme_term_ids, added_term_ids, removed_term_ids)

            query_terms_and_weights = []

            for term_id in all_term_ids:
                term_weight = 0.0

                if term_id in added_term_ids or term_id in preserved_term_ids:
                    logger.debug('Term %d: %.2f + 1.0 (present)',
                                 term_id, term_weight)

                    term_weight += 1.0

                if max_reward_document_id is not None:
                    if term_id in theme_term_ids:
                        _theme_term_weight = (
                            self.desc.qcm_scorer_desc.params.alpha * (
                                1.0 -
                                previous_serp_lm.term_frequency_for_entity(
                                    max_reward_document_id, term_id)))

                        logger.debug('Term %d: %.2f + %.2f (theme term)',
                                     term_id, term_weight, _theme_term_weight)

                        term_weight = term_weight + _theme_term_weight

                    if term_id in added_term_ids:
                        term_max_reward_prob = \
                            previous_serp_lm.term_frequency_for_entity(
                                max_reward_document_id, term_id)

                        if term_max_reward_prob == 0.0:
                            # Term was not present in the maximum reward
                            # document of the previous SERP.
                            term_idf = np.log(index.document_count()) - \
                                np.log(dictionary.dfs[term_id])

                            assert np.isfinite(term_idf)

                            _added_term_not_present_mr = (
                                self.desc.qcm_scorer_desc.params.epsilon *
                                term_idf)

                            logger.debug('Term %d (idf=%.2f): %.2f + %.2f '
                                         '(added term, not max reward)',
                                         term_id, term_idf,
                                         term_weight,
                                         _added_term_not_present_mr)

                            term_weight = (
                                term_weight + _added_term_not_present_mr)
                        else:
                            _added_term_present_mr = (
                                self.desc.qcm_scorer_desc.params.beta *
                                term_max_reward_prob)

                            logger.debug('Term %d: %.2f - %.2f '
                                         '(added term, max reward)',
                                         term_id,
                                         term_weight,
                                         _added_term_present_mr)

                            # Term present in maximum reward document of
                            # previous SERP.
                            term_weight = term_weight - _added_term_present_mr

                    if term_id in removed_term_ids:
                        _removed_term = (
                            self.desc.qcm_scorer_desc.params.delta *
                            previous_serp_lm.term_frequency_for_entity(
                                max_reward_document_id, term_id))

                        logger.debug('Term %d: %.2f - %.2f (removed term)',
                                     term_id,
                                     term_weight,
                                     _removed_term)

                        term_weight = term_weight - _removed_term

                if term_weight != 0.0:
                    query_terms_and_weights.append('{weight} {term}'.format(
                        weight=term_weight, term=dictionary[term_id]))

            if query_terms_and_weights:
                interaction_queries.append('#weight({})'.format(
                    ' '.join(query_terms_and_weights)))
            else:
                interaction_queries.append(None)

        if not filter(lambda query: query is not None, interaction_queries):
            return

        session_query = '#weight({})'.format(
            ' '.join('{interaction_weight} {interaction_query}'.format(
                     interaction_weight=np.power(
                         self.desc.qcm_scorer_desc.params.gamma,
                         len(interaction_queries) - idx),
                     interaction_query=query) if query is not None else ''
                     for idx, query in enumerate(interaction_queries)))

        f_debug_out.write(session_query)
        f_debug_out.write('\n')

        logger.debug('QCM expanded Indri query: %s', session_query)
        
        # DvD hack (if OoV and no previous info, then no results
        if session_query.replace(" ", "") == '#weight()':
            return

        query_env = None
        if self.desc.indri_scorer_desc.query_environment == \
                sesh_pb2.IndriScorerDesc.INDRI:
            results = index.query(
                query,
                document_set=map(operator.itemgetter(1),
                             index.document_ids(candidate_document_ids)))
        elif self.desc.indri_scorer_desc.query_environment == \
                    sesh_pb2.IndriScorerDesc.LM:
                query_env = pyndri.QueryEnvironment(
                        index, rules=('method:dirichlet,mu:5000',))
        elif self.desc.indri_scorer_desc.query_environment == \
                    sesh_pb2.IndriScorerDesc.TFIDF:
                query_env = pyndri.TFIDFQueryEnvironment(index)
        elif self.desc.indri_scorer_desc.query_environment == \
                    sesh_pb2.IndriScorerDesc.OKAPI:
                query_env = pyndri.pyndri.OkapiQueryEnvironment(index)
        
        if query_env:
            results = query_env.query(
                query,
                document_set=map(operator.itemgetter(1),
                                index.document_ids(candidate_document_ids)))

        logger.debug('QCM returned %d results.', len(results))

        for int_document_id, score in results:
            ext_document_id, _ = index.document(int_document_id)

            yield ext_document_id, score


SESSION_SCORERS = {
    'oracle': QrelOracleScorer,
    'indri': IndriScorer,
    'qcm': QCMScorer,
}
