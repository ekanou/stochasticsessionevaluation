# Markov Session Measures #

### Code ###
The code is written for Python 3.5  

dependencies:  
oct2py 4.0.6

The code uses parts of/is partly based on code from:  
https://github.com/cvangysel/sesh  
https://github.com/trec-dd/trec-dd-jig  

### Data ###
Session tracks 2011-2014  
Dynamic Domain tracks 2015/2016  

### Setup ###
Code and run scripts are divided into the following folders:  
1_sesh                  (Generation of runs)  
2_generation        (Score runs/generate data for MsM measures)  
3_matlab_msm    (MsM measures)  
3_msm                 (Score MsM configurations)  
4_evaluation        (Combine scores/Correlation Analysis/Discriminative Power)  
9_data                 (Runs/measurements/MsM input)  
_other  
_statistics  

### Some Running Info ###
- Sesh:  
PATH=…/trec_eval/:$PATH (not necesary anymore)  
export PYTHONPATH=…/sesh/cvangysel-common:$PYTHONPATH  
scripts/score_session_log.sh 2011 configs/all.pb …/09B/ClueWeb09_English_1/  




