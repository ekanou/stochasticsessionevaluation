#!/usr/bin/env python
from collections import defaultdict
import sys


def parse_log(filename):
    count = defaultdict(int)
    with open(filename, 'r') as log:
        sid = -1
        for line in log:
            tmp = line.split()
            if len(tmp) < 3: continue
            # If new session
            if tmp[0] != sid:
                if sid != -1:
                    count['stop'] = count['stop'] + 1
                    count['all'] = count['all'] + 1
                    index = 0
                    if positions != []:
                        for pos in positions:
                            if pos > index:
                                count['fwd'] = count['fwd']+(pos-index)
                                count['all'] = count['all']+(pos-index)
                                index = pos
                            elif pos < index:
                                count['bck'] = count['bck']+(index-pos)
                                count['all'] = count['all']+(pos-index)
                positions = []
                sid = tmp[0]
                qid = 0
                action = tmp[2]
                if action == 'Q':
                    qid = qid + 1
                    serp = tmp[5:]
                else:
                    print('Strange session starting with click')
            else:
                action = tmp[2]
                if action == 'Q':
                    index = 0
                    if positions != []:
                        for pos in positions:
                            if pos > index:
                                count['fwd'] = count['fwd']+(pos-index)
                                count['all'] = count['all']+(pos-index)
                                index = pos
                            elif pos < index:
                                count['bck'] = count['bck']+(index-pos)
                                count['all'] = count['all']+(index-pos)
                                index = pos
                    positions = []
                    serp = tmp[5:]
                    count['ref'] = count['ref'] + 1
                    count['all'] = count['all'] + 1
                if action == 'C':
                    if not tmp[3] in serp:
                        continue
                    else:
                        pos = serp.index(tmp[3])
                        positions.append(pos)
    index = 0
    if positions != []:
        for pos in positions:
            if pos > index:
                count['fwd'] = count['fwd']+(pos-index)
                count['all'] = count['all']+(pos-index)
                index = pos
            elif pos < index:
                count['bck'] = count['bck']+(index-pos)
                count['all'] = count['all']+(index-pos)
    count['stop'] = count['stop'] + 1
    count['all'] = count['all'] + 1
    return count

def main():
    count = parse_log('/Users/ekanou/YandexClicks.txt')
    print(count['fwd'], count['bck'], count['ref'], count['stop'], count['all'])
    p = count['fwd']/count['all']
    b = count['bck']/count['all']
    r = count['ref']/count['all']
    s = count['stop']/count['all']
    print("Fwd prob: %0.2f Bck prob: %0.2f Reform prob: %0.2f Stop prob: %0.2f"
          % (p, b, r, s))

if __name__ == "__main__":
    sys.exit(main())
