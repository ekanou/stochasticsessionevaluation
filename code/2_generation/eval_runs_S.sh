#!/usr/bin/env bash


python eval_runs.py \
    --runs_path "../9_data/runs/S" \
    --out_path "../9_data/measurements"\
    --cutoff 10 \
    --list_depth 10 \
    --track "S" \


